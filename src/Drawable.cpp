/*****************************************************************************
 *
 * Dytraed - Dynamic Treancrip Editor/Sphinx Visualizer GL
 *
 * � 2007 Bo Zhang, Engineering Dept, University of Waikato, New Zealand
 *        Saturnino Luz, Trinity College Dublin, Ireland
 *
 *
 ****************************************************************************/
#include <iostream>
#include <sstream>
#include <stack>
#include "Drawable.hpp"

using namespace std;

#define SVGL_TEXT_SIZE		0.1f

SVWord::SVWord(Word &w, GLfloat c[4], SVLayer *p)
{
  TargetPosX = CurrentPosX = w.Time;
  TargetPosY = CurrentPosY = -20.0f;
	
  TargetOpacity = 1.0f;
  CurrentOpacity = 0.0f;
	
  WordData = w;
	
  IsQuadVisible = false;

  memcpy(TargetColor, c, sizeof (GLfloat) * 4);
  memcpy(CurrentColor, c, sizeof (GLfloat) * 4);
	
  /* Totally transparent */
  CurrentColor[3] = 0;
	
  Parent = p;
}

void SVWord::showQuad(bool bShow)
{
  IsQuadVisible = bShow;
}

void SVWord::setPositionX(float x)
{
  TargetPosX = x;
}

void SVWord::setPositionXImmed(float x)
{
  CurrentPosX = TargetPosX = x;
}

void SVWord::setPositionY(float y)
{
  TargetPosY = y;
}

void SVWord::setPositionYImmed(float y)
{
  CurrentPosY = TargetPosY = y;
}

void SVWord::setOpacity(float o)
{
  TargetOpacity = o;
}

void SVWord::setColor(GLfloat c[4])
{
  memcpy(TargetColor, c, sizeof (GLfloat) * 4);
}

void SVWord::setQuadColor(GLfloat c[4])
{
  memcpy(QuadColor, c, sizeof (GLfloat) * 4);
}

void SVWord::setLink(int n)
{
  Link = n;
}

void SVWord::clearLink()
{
  Link = 0;
}

void SVWord::incrementLink()
{
  Link ++;
}

int SVWord::getLink()
{
  return Link;
}

float SVWord::getOpacity() const
{
  return CurrentOpacity;
}

void SVWord::updateFrame(float time)
{
  /* Update X position */
  ValueApproachExp(CurrentPosX, TargetPosX, time, 80);
  /* Update Y position */
  ValueApproachExp(CurrentPosY, TargetPosY, time, 40);
  /* Update transparency */
  ValueApproachLinear(CurrentOpacity, TargetOpacity, time, 1.4f);
  /* Update Quad transparency based on show flag) */
  ValueApproachLinear(QuadColor[3], (IsQuadVisible ? CurrentColor[3] * 0.5f : 0), time, 1.4f);
}

void SVWord::draw(Graphics *g)
{
  if (QuadColor[3] > 0.005f) {	
    /* Display a rectangle around text for selection */	
    GLfloat v0[3], v1[3], v2[3], v3[3];
    generateQuad(v0, v1, v2, v3);
    g->setColor(QuadColor);
    g->drawQuad(v0, v1, v2, v3);
  }

  if (CurrentOpacity < 0.005f) return;
	
  GLfloat pos[3];
  getCompositedPosition(pos);
  CurrentColor[3] = (IsQuadVisible ? Parent->getOpacity() : CurrentOpacity * Parent->getOpacity());
  g->setColor(CurrentColor);
  g->drawText(pos[0], pos[1], pos[2], WordData.Spell, SVGL_TEXT_SIZE);	
}

/* Test against word quad and a given ray in 3D space */
bool SVWord::hitTest(GLfloat p1[3], GLfloat p2[3])
{
  GLfloat v0[3], v1[3], v2[3], v3[3];
  generateQuad(v0, v1, v2, v3);
	
  GLfloat ret;

  if (rayTriangleIntersect(p1, p2, v0, v1, v2, &ret)) return true;
  if (rayTriangleIntersect(p1, p2, v0, v2, v3, &ret)) return true;

  return false;
}

void SVWord::generateQuad(GLfloat *v0, GLfloat *v1, GLfloat *v2, GLfloat *v3)
{
  GLfloat pos[3], pos2[3];
	
  getCompositedPosition(pos);

  pos2[0] = pos[0] + measureText(WordData.Spell, SVGL_TEXT_SIZE);
  pos2[1] = pos[1] + SVGL_TEXT_SIZE * 110;
  pos2[2] = pos[2];
	
  pos[1] -= SVGL_TEXT_SIZE * 40;
	
  v0[0] = pos[0]; v0[1] = pos2[1]; v0[2] = pos[2] - 0.5f;
  v1[0] = pos[0]; v1[1] = pos[1]; v1[2] = pos[2] - 0.5f;
  v2[0] = pos2[0]; v2[1] = pos[1]; v2[2] = pos[2] - 0.5f;
  v3[0] = pos2[0]; v3[1] = pos2[1]; v3[2] = pos[2] - 0.5f;
}

void SVWord::getCompositedPosition(GLfloat p[3])
{
  p[0] = CurrentPosX;
  p[1] = CurrentPosY;
  p[2] = Parent->getPositionZ();	
}

/****************************************************************************/

void SVLayer::setPositionZ(float z)
{
  TargetPosZ = z;
}

float SVLayer::getPositionZ() const
{
  return CurrentPosZ;
}

float SVLayer::getTotalPathLength() const
{
  return TotalPathLen;
}

/* Initialize resource for new snapshot */
void SVLayer::beginSnapshot(Graphics *g)
{
  if (Finalized) return;
	
  GraphicsDev = g;
  TotalPathLen = 0;
	
  /* Clear link counter */
  vector<SVWord *>::iterator iter;
  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    (*iter)->clearLink();
  }

  clearBestPath();
}

void SVLayer::addPath(const Path *p)
{
  if (Finalized) 
    return;
  addPathTo(p, &PathList);
}

void SVLayer::addBestPath(const Path *p)
{
  if (Finalized) 
    return;
  addPathTo(p, &BestPath);
}

/* Finalizing snapshot */
void SVLayer::endSnapshot()
{	
  if (Finalized) 
    return;
	
  rehashWords();
  float w = 0;
  int i;
  /* Re-place word position based on hash bucket size */
  vector<SVWordHashTable *>::iterator iter;

  for (iter = HashList.begin(); iter != HashList.end(); iter ++) {
    SVWordHashTable *item = (*iter);
    float maxhalf = item->Count * 10;
		
    for (i = 0; i < item->Count; i ++) {
      float xdiff = item->Words[i]->WordData.Time - item->Time;
      item->Words[i]->setPositionX(w + xdiff);
      item->Words[i]->setPositionY(i * 20 - maxhalf);
      if (item->Words[i]->getLink() <= 0) {
        // Show word as inactive */
        item->Words[i]->setOpacity(0.3f);
        item->Words[i]->showQuad(false);
      } 
      else {
        item->Words[i]->setOpacity(1);
      }
    }
    w += item->MaxWidth + 100 * SVGL_TEXT_SIZE;
  }
  TotalPathLen = w;
}

/* Left the overriden best path only */
void SVLayer::finalizeLayer(Path *BestPathOverride)
{
  if (Finalized) 
    return;
	
  beginSnapshot(GraphicsDev);
  addBestPath(BestPathOverride);
  endSnapshot();
  //cout << "Override layer path: " << BestPathOverride->toString() << endl;
  realFinalizeLayer(true);
}

void SVLayer::finalizeLayer()
{
  realFinalizeLayer(false);
}

/* Left the best path only */
void SVLayer::realFinalizeLayer(bool bImmedMove)
{
  if (Finalized)
    return;
	
  Finalized = true;
  vector<SVWord *>::iterator iter;

  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    (*iter)->setOpacity(0);
    (*iter)->showQuad(0);
  }
	
  float w, width = 0;	
  vector<Edge *>::iterator eiter;

  for (eiter = BestPath.begin(); eiter != BestPath.end(); eiter ++) {
    (*eiter)->Start->setOpacity(1);
    (*eiter)->End->setOpacity(1);		
    if (!bImmedMove) {
      (*eiter)->Start->setPositionY(0);
      (*eiter)->End->setPositionY(0);
    } 
    else {
      (*eiter)->Start->setPositionYImmed(0);
      (*eiter)->End->setPositionYImmed(0);		
    }
				
    if (eiter == BestPath.begin()) {
      if (!bImmedMove) {
        (*eiter)->Start->setPositionX(width);
      } 
      else {
        (*eiter)->Start->setPositionXImmed(width);
      }
      w = measureText((*eiter)->Start->WordData.Spell, SVGL_TEXT_SIZE);
      width += w + SVGL_TEXT_SIZE * 100;
    }
    if (!bImmedMove) {
      (*eiter)->End->setPositionX(width);
    } 
    else {
      (*eiter)->End->setPositionXImmed(width);
    }
    w = measureText((*eiter)->End->WordData.Spell, SVGL_TEXT_SIZE);
    width += w + SVGL_TEXT_SIZE * 100;
  }
  clearPathList();	
}

bool SVLayer::isFinalized() const
{
  return Finalized;
}

void SVLayer::updateFrame(float time)
{
  /* Update Z position */
  ValueApproachExp(CurrentPosZ, TargetPosZ, time, 10);
  /* Update opacity */
  ValueApproachLinear(CurrentOpacity, TargetOpacity, time, 2);
  /* Update each word */
  vector<SVWord *>::iterator iter;

  for (iter = WordList.begin(); iter != WordList.end(); iter ++){
    SVWord *w = (*iter);
    w->updateFrame(time);
  }
}

void SVLayer::draw(Graphics *g)
{
  if (CurrentOpacity < 0.01f)
    return;
	
  /* Draw each word */
  vector<SVWord *>::iterator iter;
  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    (*iter)->draw(g);
  }
	
  /* Draw all paths */
  GLfloat EdgeColor[4] = {1, 1, 0.2f, 0};
  drawPath(g, &PathList, EdgeColor);
	
  /* Draw best path */
  GLfloat BestColor[4] = {1, 0, 0, 0};
  drawPath(g, &BestPath, BestColor);
}

void SVLayer::setOpacity(float o)
{
  TargetOpacity = o;
}

float SVLayer::getOpacity() const
{
  return CurrentOpacity;
}

SVLayer::SVLayer()
{
  Finalized = false;
  TargetPosZ = CurrentPosZ = 0.0f;
  CurrentOpacity = 1.0f;
  TargetOpacity = 1.0f;
}

SVLayer::~SVLayer()
{
  clearWords();
  clearPaths();
}

void SVLayer::drawPath(Graphics *g, vector<Edge *> *tPath, GLfloat color[4])
{
  GLfloat EdgeStart[3];
  GLfloat EdgeEnd[3];
	
  /* Draw edges */
  vector<Edge *>::iterator iter;
  for (iter = tPath->begin(); iter != tPath->end(); iter ++) {
    Edge *e = (*iter);
		
    float op1 = e->Start->getOpacity();
    float op2 = e->End->getOpacity();
	
    if (op1 > op2) op1 = op2;
		
    /* Opacity of an edge equals minimal opacity of two referenced words */
    color[3] = op1 * getOpacity();
		
    e->Start->getCompositedPosition(EdgeStart);
    e->End->getCompositedPosition(EdgeEnd);
		
    g->setColor(color);
		
    g->drawLine(EdgeStart, EdgeEnd, 1.0f);
  }
}

/* Clear hash list and hash all words in the word list */
void SVLayer::rehashWords()
{
  clearHashList();
	
  /* Put each word in appropriate hash bucket */
  vector<SVWord *>::iterator iter;
  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    hashProbe((*iter));
  }
	
  /* TODO: Sort the buckets by time, or do we really need to do it ?! */
}

/* Hash a word. If the buck does not exist in the hash list, create the new bucket */
int SVLayer::hashProbe(SVWord *w)
{
  float width = 0;
  float fw = measureText(w->WordData.Spell, SVGL_TEXT_SIZE);
	
  vector<SVWordHashTable *>::iterator iter;
  for (iter = HashList.begin(); iter != HashList.end(); iter ++) {
		
    SVWordHashTable *item = (*iter);
		
    if (abs(item->Time - w->WordData.Time) < 10) {
      /* Goes to the same bucket */
      if (item->Count < SVWORD_HASH_SIZE) {
        item->Words[item->Count ++] = w;
				
        if (item->MaxWidth < fw) item->MaxWidth = fw;
				
        /* Found */
        return 1;
      } else {
        /* Error: bucket full */
        return -1;
      }
    }
  }
	
  /* Not found, add new bucket */
  SVWordHashTable *newitem = new SVWordHashTable();
	
  newitem->Time = w->WordData.Time;
  newitem->Count = 0;
  newitem->Words[newitem->Count ++] = w;
  newitem->Generation = w->WordData.Generation;
  newitem->MaxWidth = fw;
  HashList.push_back(newitem);
	
  return 0;
}

/* See if a given word exists in the word list. If yes, increment the reference 
 * counter and return the word. Otherwise create a new word using given info and
 * initialize the reference counter to 1, add the new word to the word list, return
 * the item */
SVWord* SVLayer::wordProbe(Word &w)
{
  vector<SVWord *>::iterator iter;
  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    SVWord *item = (*iter);
		
    if (item->WordData == w) {
      item->incrementLink();
      /* Found, link incremented */
      return item;
    }
  }
  static GLfloat WordColor[4] = {1, 1, 1, 0};
  /* Not found, create */
  SVWord *newitem = new SVWord(w, WordColor, this);
  newitem->incrementLink();
	
  static GLfloat QuadColor[4] = {0, 0, 1, 0};
	
  newitem->setQuadColor(QuadColor);
  newitem->showQuad(false);
	
  WordList.push_back(newitem);
	
  return newitem;
}

void SVLayer::addPathTo(const Path *p, vector<Edge *> *ToPath)
{
  int len = p->size();
  SVWord *pred = NULL;
	
  for (int i = 0; i < len; i ++) {
    /* Extract words from path */
    Word w = p->getWord(i);
    /* Add word to WordList if not found, or increment link counter if found */
    /* A.k.a: probe the word */
    SVWord *curr = wordProbe(w);
    /* Add Edge to list */
    if (pred != NULL) {
      Edge *e = new Edge();
      e->Start = pred;
      e->End = curr;
      ToPath->push_back(e);
      //cout << "Adding Edge: " << pred->WordData.Spell << " -> " << curr->WordData.Spell << endl;
    }
    pred = curr;
  }
}

void SVLayer::clearWords()
{
  /* Hash list references words created here, we must clear it first */
  clearHashList();
  vector<SVWord *>::iterator iter;
  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    delete (*iter);
  }
  WordList.clear();
}

void SVLayer::clearPathList()
{
  vector<Edge *>::iterator iter;
  for (iter = PathList.begin(); iter != PathList.end(); iter ++) {
    delete (*iter);
  }
  PathList.clear();
}

void SVLayer::clearBestPath()
{
  vector<Edge *>::iterator iter;
  for (iter = BestPath.begin(); iter != BestPath.end(); iter ++) {
    delete (*iter);
  }
  BestPath.clear();
}
	
void SVLayer::clearPaths()
{
  clearPathList();
  clearBestPath();
}

void SVLayer::clearHashList()
{
  vector<SVWordHashTable *>::iterator iter;
  for (iter = HashList.begin(); iter != HashList.end(); iter ++) {
    delete (*iter);
  }
  HashList.clear();
}

SVWord *SVLayer::onRayClick(GLfloat *p1, GLfloat *p2)
{
  SVWord *ret = NULL;
	
  // Do hit test to each word
  vector<SVWord *>::iterator iter;
  for (iter = WordList.begin(); iter != WordList.end(); iter ++) {
    SVWord *w = (*iter);
		
    /* We can't select (inactive) words without a link */
    if (w->getLink() > 0 && w->hitTest(p1, p2)) {
      w->showQuad(true);
      /* Actions:
       *  If not an auto filled word and AutoFiller result is not shown: 
       *	show AutoFiller results
       *  else do sentence completion
       */	 
      ret = w;
    } else {
      w->showQuad(false);
    }
  }
  return ret;
}

/* Trace a sentence ends with the given word.
 * The word [end] must came from onRayClick and shouldn't be
 * modified or deleted
 */
string SVLayer::reverseTracePath(SVWord *last)
{
  stack<SVWord *> path;

  SVWord *end;
  bool bFound;
	
  // Look for best-path first as it is simpler and more likely to be used
  end = last;
  do {
    bFound = false;
		
    vector<Edge *>::iterator iter;
    for (iter = BestPath.begin(); iter != BestPath.end(); iter ++) {
      if ((*iter)->End == end) {
        bFound = true;
        path.push(end);
        end = (*iter)->Start;
        break;
      }
    }
		
  } while(bFound);
	
  if (end == last) {
    // Look for all paths
    end = last;
    do {
      bFound = false;
			
      vector<Edge *>::iterator iter;
      for (iter = PathList.begin(); iter != PathList.end(); iter ++) {
        if ((*iter)->End == end) {
          bFound = true;
          path.push(end);
          end = (*iter)->Start;
          break;
        }
      }
    } while(bFound);
  }
	
  if (end == last) return "";
	
  path.push(end);
	
  // Unwind stack
  stringstream ss;
	
  while (path.size() > 0) {
    ss << path.top()->WordData.Spell;
    path.pop();
    if (path.size() > 0) ss << " ";
  }	
  return ss.str();
}

/****************************************************************************/

void SVView::setAutoFiller(AutoFiller *f)
{
  Filler = f;
  AutoFilledCount = 0;
}

void SVView::clearLayers()
{
  //cout << "Function Call: " << __func__ << endl;
  vector<SVLayer *>::iterator iter;

  for (iter = Layers.begin(); iter != Layers.end(); iter ++) {
    delete (*iter);
  }
	
  Layers.clear();
	
  if (ActiveLayer != NULL) {
    delete ActiveLayer;
    ActiveLayer = NULL;
  }
}

void SVView::newLayer()
{
  //cout << "Function Call: " << __func__ << endl;
	
  if (ActiveLayer != NULL) {
	
    ActiveLayer->finalizeLayer();
		
    /* If history queue full, remove oldest layer */
    if (Layers.size() >= SVVIEW_MAX_LAYERS) {
      SVLayer *old = Layers.back();					
      Layers.pop_back();
      delete old;
    }
		
    /* Move current active layer to normal layers */
    Layers.insert(Layers.begin(), ActiveLayer);

    for (int i = 0; i < Layers.size(); i ++) {
      Layers[i]->setPositionZ(-i * 40 - 60);
      Layers[i]->setOpacity((8.0f / 12.0f - ((float)i / 12.0f)) / 2);
    }
  }
	
  AutoFilledCount = 0;
  TracingPath = "";
	
  ActiveLayer = new SVLayer();
}

int SVView::applySnapshot(Snapshot *s, Graphics *g)
{
  if (PausedForAction || ActiveFillCountDown > 1)
    return SVVIEW_SNAPSHOT_RETRY;	/* return: Try again */
  //cout << "snaps: " << s->toString() << endl;
  //cout << "type: " << s->getType() << endl;
  if (s->getType() == LT_BEGIN) {
    /* LT_BEGIN marks begin of a new layer (a layer can have several snapshots) */
    newLayer();
  }
	
  if (ActiveLayer != NULL && ActiveLayer->isFinalized()) 
    return SVVIEW_SNAPSHOT_JUMP;
	
  if (ActiveLayer != NULL) {
    ActiveLayer->beginSnapshot(g);
    //cout << "size: " << s->size() << endl;

    for (int i = 0; i < s->size(); i ++) {
      const Path *p = s->getPath(i);
      //cout << "path: " << p << endl;
   
      if (i > 0) {
        ActiveLayer->addPath(p);
      } else {
        ActiveLayer->addBestPath(p);
      }
    }
		
    ActiveLayer->endSnapshot();
		
    /* Update camera position to make sure entire sentence is visible */
    float sw = ActiveLayer->getTotalPathLength();		
    float w = (800 - sw) / (800 - 300);
    
    /* w = (w <= 0 )? 0 : 1;*/
    if (w < 0 ) 
      w = 0; 
    else if (w > 1) 
      w = 1;
		
    float cx, cy, cz;
		
    cx = Lerp(-120, 30, w);
    cy = Lerp(130, 90, w);
    cz = Lerp(420, 220, w);
    
    g->setViewPosition(cx, cy, cz);
		
    cx = Lerp(170, 120, w);
    cy = Lerp(0, 0, w);
    cz = Lerp(100, 0, w);
		
    g->setViewTarget(cx, cy, cz);
  }
  return SVVIEW_SNAPSHOT_OK;
}

int SVView::onRayClick(GLfloat *p1, GLfloat *p2)
{
  SVLayer *l = ActiveLayer;

  if (l != NULL) {
    // Test against AutoFillPhrase[]
          
    for (int i = 0; i < AutoFilledCount; i ++) {
      if (autoFillHitTest(i, p1, p2)) {
        /* Hit an auto filled text, complete sentence
         * and advance snapshot
         */
        //cout << "Suggest taken: " << TracingPath << " " << AutoFilledPhrase[i] << endl;           
        // Initiate ActiveFill animation
        float wfinal = measureText(TracingPath, SVGL_TEXT_SIZE) + 90 * SVGL_TEXT_SIZE;
        ActiveFill = i;
              
        TracingPathTargetPos[0] = 0;
        TracingPathTargetPos[1] = 0;
        TracingPathTargetPos[2] = 0;
        TracingPathTargetPos[3] = 100;
				
        TracingPathCurrPos[0] = 0;
        TracingPathCurrPos[1] = -50;
        TracingPathCurrPos[2] = 0;
        TracingPathCurrPos[3] = 50;
				
        ActiveFillTargetPos[0] = wfinal;
        ActiveFillTargetPos[1] = 0;
        ActiveFillTargetPos[2] = 0;
        ActiveFillTargetPos[3] = 100;
				
        ActiveFillCurrPos[0] = wfinal / 2;
        ActiveFillCurrPos[1] = -50 - i * 10;
        ActiveFillCurrPos[2] = 0;
        ActiveFillCurrPos[3] = 50;
				
        ActiveFillCountDown = 2.0f;
				
        ActiveFillTargetOpa = 0.0f;
        ActiveFillCurrOpa = 1.0f;
				
        PausedForAction = false;
        TracingTargetOpa = 0;
				
        return 3;
      }
    }
	
    // Test against layer words
    SVWord *r = (l->isFinalized() ? NULL : l->onRayClick(p1, p2));
		
    if (r != NULL) {
      TracingPath = l->reverseTracePath(r);
      TracingPathWidth = measureText(TracingPath, SVGL_TEXT_SIZE) + 90 * SVGL_TEXT_SIZE;
			
      if (TracingPath.size() == 0) return 1;
      // Invoke auto-filler
      AutoFiller *f = Filler;
			
      if (f != NULL) {
        Sentence st;	
        st.parse(TracingPath);
        int res = f->findSuggestions(st, 8);
        //cout << "Suggestions found: " << res << endl;
        int pos = 0, i, j;
        if (res > 8) res = 8;
        for (i = 0; i < res; i ++) {
          Sentence *seg = f->getSuggestion(i, pos);					
          stringstream sm;
          for (j = pos + 1; j < seg->size(); j ++) {
            sm << seg->getWord(j)->Spell;
            sm << " ";
          }
          //cout << "Suggestion #" << i << ": " << sm.str() << endl;
          AutoFilledPhrase[i] = sm.str();
        }
        AutoFilledCount = res;
      }
      TracingTargetOpa = 1;
      PausedForAction = true;
      return 2;
    } 
    else {
      TracingTargetOpa = 0;
      PausedForAction = false;
    }
  }
  return 0;
}

const SVLayer *SVView::getActiveLayer()
{
  return ActiveLayer;
}

void SVView::updateFrame(float time)
{
  //cout << "Function Call: " << __func__ << endl;
  vector<SVLayer *>::iterator iter;

  for (iter = Layers.begin(); iter != Layers.end(); iter ++) {
    (*iter)->updateFrame(time);
  }
  if (ActiveLayer != NULL) {
    ActiveLayer->updateFrame(time);
  }
  ValueApproachLinear(TracingCurrOpa, TracingTargetOpa, time, 3.0f);
	
  if (ActiveFillCountDown > 0 && ActiveFill < AutoFilledCount) {
    ValueApproachExpPrecise(TracingPathCurrPos[0], TracingPathTargetPos[0], time, 40);
    ValueApproachExpPrecise(TracingPathCurrPos[1], TracingPathTargetPos[1], time, 40);
    ValueApproachExpPrecise(TracingPathCurrPos[2], TracingPathTargetPos[2], time, 40);
    ValueApproachExpPrecise(TracingPathCurrPos[3], TracingPathTargetPos[3], time, 40);
		
    ValueApproachExpPrecise(ActiveFillCurrPos[0], ActiveFillTargetPos[0], time, TracingPathWidth / 2);
    ValueApproachExpPrecise(ActiveFillCurrPos[1], ActiveFillTargetPos[1], time, 40 + ActiveFill * 10);
    ValueApproachExpPrecise(ActiveFillCurrPos[2], ActiveFillTargetPos[2], time, 40);
    ValueApproachExpPrecise(ActiveFillCurrPos[3], ActiveFillTargetPos[3], time, 40);
		
    ValueApproachLinear(ActiveFillCurrOpa, ActiveFillTargetOpa, time, 0.8f);		
    //cout << "TargetSize: " << TracingPathCurrPos[3] << endl;
    ActiveFillCountDown -= time;
		
    if (ActiveLayer != NULL) {
      if (ActiveFillCountDown > 1) {
        ActiveLayer->setOpacity(0);
        if (ActiveFillCountDown < 1.5f && !ActiveLayer->isFinalized()) {
          Path p;
          string s = TracingPath + " " + AutoFilledPhrase[ActiveFill];			
          StringToPath(&p, s);
          ActiveLayer->finalizeLayer(&p);
        }
      } else {
        ActiveLayer->setOpacity(1);
      }
    }
  }
}

void SVView::draw(Graphics *g)
{
  //cout << "Function Call: " << __func__ << endl;
  vector<SVLayer *>::iterator iter;	
  for (iter = Layers.begin(); iter != Layers.end(); iter ++) {
    (*iter)->draw(g);
  }
	
  if (ActiveLayer != NULL) {
    ActiveLayer->draw(g);
  }
	
  static GLfloat pathColor[] = {0, 0.3f, 1, 0.6f};
  static GLfloat suggColor[] = {0, 1, 1, 0.6f};
  static GLfloat noSuggColor[] = {1, 0, 0, 0.6f};
  static GLfloat aniColor[] = {1, 1, 1, 1};
	
  pathColor[3] = TracingCurrOpa;
  suggColor[3] = TracingCurrOpa;
  noSuggColor[3] = TracingCurrOpa;
	
  static GLfloat v1[3], v2[3], v3[3], v4[3];
	
  g->setColor(pathColor);
  g->drawText(0, -50, 0, TracingPath, SVGL_TEXT_SIZE / 2);

  float w = measureText(TracingPath, SVGL_TEXT_SIZE / 2) + 50 * SVGL_TEXT_SIZE;
	
  if (TracingPath.size() > 0) {
    float p1x, p1y, p2x, p2y;		
    p1x = 0;	p1y = -52;
    p2x = w;	p2y = -52 - 2;
		
    v1[0] = p1x;	v1[1] = p1y;	v1[2] = 0;
    v2[0] = p1x;	v2[1] = p2y;	v2[2] = 0;
    v3[0] = p2x;	v3[1] = p2y;	v3[2] = 0;
    v4[0] = p2x;	v4[1] = p1y;	v4[2] = 0;
		
    g->setColor(noSuggColor);
    g->drawLine(v1, v4, 2);
  }
	
  if (AutoFilledCount > 0) {
    for (int i = 0; i < AutoFilledCount; i ++) {
      suggColor[3] = (1 - (float)i / AutoFilledCount) * TracingCurrOpa;
      g->setColor(suggColor);
      g->drawText(w + 5, -50 - i * 10, 0, AutoFilledPhrase[i], SVGL_TEXT_SIZE / 2);
    }
		
    // Draw a vertical line at the end of sentence
    float p1x, p1y, p2x, p2y;
		
    p1x = w;	p1y = -45;
    p2x = w + 3;	p2y = -42 - AutoFilledCount * 10;
		
    v1[0] = p1x;	v1[1] = p1y;	v1[2] = 0;
    v2[0] = p1x;	v2[1] = p2y;	v2[2] = 0;
    v3[0] = p2x;	v3[1] = p2y;	v3[2] = 0;
    v4[0] = p2x;	v4[1] = p1y;	v4[2] = 0;
		
    suggColor[3] = 0.6f * TracingCurrOpa;
    g->setColor(suggColor);
    g->drawQuad(v1, v2, v3, v4);
		
  } else if (TracingPath.size() > 0) {
    // Draw a red cross at the end of sentence
    float p1x, p1y, p2x, p2y;
		
    p1x = w;	p1y = -46;
    p2x = w + 5;	p2y = p1y - 5;
		
    v1[0] = p1x;	v1[1] = p1y;	v1[2] = 0;
    v2[0] = p1x;	v2[1] = p2y;	v2[2] = 0;
    v3[0] = p2x;	v3[1] = p2y;	v3[2] = 0;
    v4[0] = p2x;	v4[1] = p1y;	v4[2] = 0;
				
    g->setColor(noSuggColor);
    g->drawLine(v1, v3, 5);
    g->drawLine(v2, v4, 5);
  }
	
  if (ActiveFillCountDown > 0 && ActiveFill < AutoFilledCount) {
    aniColor[3] = ActiveFillCurrOpa;
    g->setColor(aniColor);
    g->drawText(TracingPathCurrPos[0], 
                TracingPathCurrPos[1], 
                TracingPathCurrPos[2], 
                TracingPath, TracingPathCurrPos[3] / 100 * SVGL_TEXT_SIZE);
    g->drawText(ActiveFillCurrPos[0], 
                ActiveFillCurrPos[1],
                ActiveFillCurrPos[2],
                AutoFilledPhrase[ActiveFill],
                ActiveFillCurrPos[3] / 100 * SVGL_TEXT_SIZE);
  }
}

/* Test against auto-fill phrase quad and a given ray in 3D space */
bool SVView::autoFillHitTest(int id, GLfloat *p1, GLfloat *p2)
{
  float w = measureText(TracingPath, SVGL_TEXT_SIZE / 2) + 50 * SVGL_TEXT_SIZE;
  float wph = measureText(AutoFilledPhrase[id], SVGL_TEXT_SIZE / 2);
	
  float p1x, p1y, p2x, p2y;
		
  p1x = w;	p1y = -46 - id * 10;
  p2x = w + wph;	p2y = p1y - 5;
	
  GLfloat v0[3], v1[3], v2[3], v3[3];
	
  v0[0] = p1x;	v0[1] = p1y;	v0[2] = 0;
  v1[0] = p1x;	v1[1] = p2y;	v1[2] = 0;
  v2[0] = p2x;	v2[1] = p2y;	v2[2] = 0;
  v3[0] = p2x;	v3[1] = p1y;	v3[2] = 0;
	
  GLfloat ret;

  if (rayTriangleIntersect(p1, p2, v0, v1, v2, &ret)) return true;
  if (rayTriangleIntersect(p1, p2, v0, v2, v3, &ret)) return true;

  return false;
}

SVView::SVView()
{
  PausedForAction = false;
  ActiveLayer = NULL;
  Filler = NULL;
  AutoFilledCount = 0;
  TracingPathWidth = 0;
  TracingCurrOpa = 0;
  TracingTargetOpa = 0;
  ActiveFillCurrOpa = 0;
  ActiveFillTargetOpa = 0;
}

SVView::~SVView()
{
  clearLayers();
}


void StringToPath(Path *p, string &s)
{
  stringstream ss;
  int i = 0;
	
  cout << "StringToPath: ";
  ss << s;
	
  while (!ss.eof()) {
    string sw;
    ss >> sw;
		
    if (ss.tellg() < 0) break;
		
    Word w;
    w.Spell = sw;
    w.Time = 20 * i ++;
    w.Score = 0;
		
    p->addWord(w);
		
    cout << sw << " ";
  }
	
  cout << endl;
}
