/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 *
 ****************************************************************************/

#include <string>
#include <vector>
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include "Elements.hpp"
#include "Graphics.hpp"
#include "Snapshot.hpp"
#include "Language.hpp"

using namespace std;

#ifndef _SVGL_DRAWABLE_
#define _SVGL_DRAWABLE_

#define SVVIEW_MAX_LAYERS	8
#define SVWORD_HASH_SIZE	12

#define SVVIEW_SNAPSHOT_OK	0
#define SVVIEW_SNAPSHOT_RETRY	1
#define SVVIEW_SNAPSHOT_JUMP	2

class SVWord;
class SVLayer;
class SVView;
struct SVWordHashTable;

/* Stores a renderable word with position info */
class SVWord {	
	public: SVWord(Word &w, GLfloat c[4], SVLayer *p);
	
	public: bool hitTest(GLfloat p1[3], GLfloat p2[3]);
	public: void showQuad(bool bShow);
	public: void setPositionX(float x);
	public: void setPositionXImmed(float x);
	public: void setPositionY(float y);
	public: void setPositionYImmed(float y);
	public: void setOpacity(float o);
	public: void setColor(GLfloat c[4]);
	public: void setQuadColor(GLfloat c[4]);
	public: void setLink(int n);
	public: void clearLink();
	public: void incrementLink();
	public: int getLink();
	public: float getOpacity() const;
	
	public: void getCompositedPosition(GLfloat p[3]);
	
	public: void updateFrame(float time);
	public: void draw(Graphics *g);

	public: Word	WordData;

	
	private: void generateQuad(GLfloat *v0, GLfloat *v1, GLfloat *v2, GLfloat *v3);
	private: float	CurrentPosX;
	private: float	TargetPosX;
	private: float	CurrentPosY;
	private: float	TargetPosY;
	private: float	CurrentOpacity;
	private: float	TargetOpacity;
	private: bool	IsQuadVisible;
	private: GLfloat	CurrentColor[4];
	private: GLfloat	TargetColor[4];
	private: GLfloat	QuadColor[4];
	private: int	Link;
	private: SVLayer *Parent;
};

/* Stores a single edge connects two words */
struct Edge {
	public: SVWord *Start;
	public: SVWord *End;
};

/* A bucket in word hash table */
struct SVWordHashTable {
	int	Time;
	int	Count;
	float	MaxWidth;
	int	Generation;
	/* For simplicity's sake we choose a fixed bucket capacity */
	SVWord*	Words[SVWORD_HASH_SIZE];
};

/* A renderable layer represents a complete sentence, consists 
 * multiple words and paths */
class SVLayer {
	public: void setPositionZ(float z);
	public: float getPositionZ() const;	
	public: void setOpacity(float o);
	public: float getOpacity() const;
	public: float getTotalPathLength() const;
	
	public: SVWord *onRayClick(GLfloat *p1, GLfloat *p2);
	public: void beginSnapshot(Graphics *g);
	public: void addPath(const Path *p);
	public: void addBestPath(const Path *p);
	public: void endSnapshot();
	public: bool isFinalized() const;
	
	public: void finalizeLayer();
	public: void finalizeLayer(Path *BestPathOverride);
	
	public: void updateFrame(float time);
	public: void draw(Graphics *g);
	
	public: string reverseTracePath(SVWord *last);
	
	public: SVLayer();
	public: ~SVLayer();
	
	private: void realFinalizeLayer(bool bImmedMove);
	private: void clearWords();
	private: void clearPaths();
	private: void clearPathList();
	private: void clearBestPath();
	private: void clearHashList();
	private: SVWord* wordProbe(Word &w);
	private: void addPathTo(const Path *p, vector<Edge *> *ToPath);
	private: void drawPath(Graphics *g, vector<Edge *> *tPath, GLfloat color[4]);
	private: void rehashWords();
	private: int hashProbe(SVWord *w);
	
	private: Graphics *		GraphicsDev;
	private: vector<SVWord *>	WordList;
	private: vector<Edge *>		PathList;
	private: vector<Edge *>		BestPath;
	private: vector<SVWordHashTable *>	HashList;
	private: bool	Finalized;
	private: float	TotalPathLen;
	private: int	CurrentGen;
	private: float	CurrentPosZ;
	private: float	TargetPosZ;
	private: float	CurrentOpacity;
	private: float	TargetOpacity;	
	private: float	TimeStart;
	private: float	TimeEnd;
};

/* A renderable view represents a set of layers sorted in timely order */
class SVView {
	public: void setAutoFiller(AutoFiller *f);
	public: void clearLayers();
	public: int applySnapshot(Snapshot *s, Graphics *g);
	public: const SVLayer *getActiveLayer();
	public: void updateFrame(float time);
	public: void draw(Graphics *g);
	public: void newLayer();
	public: int onRayClick(GLfloat *p1, GLfloat *p2);

	public: SVView();
	public: ~SVView();
	
	private: bool autoFillHitTest(int id, GLfloat *p1, GLfloat *p2);
	
	private: bool		PausedForAction;
	private: AutoFiller *	Filler;
	private: string		TracingPath;
	private: string		AutoFilledPhrase[8];
	private: int		AutoFilledCount;
	private: vector<SVLayer *>	Layers;
	private: SVLayer *	ActiveLayer;
	
	private: float		ActiveFillCountDown;
	/* The following 'things' are used to do ActiveFill confirmation animation */
	private: int		ActiveFill;
	private: GLfloat	TracingPathCurrPos[4];		/* x,y,z,scale */
	private: GLfloat	TracingPathTargetPos[4];	/* x,y,z,scale */
	
	private: GLfloat	ActiveFillCurrPos[4];		/* x,y,z,scale */
	private: GLfloat	ActiveFillTargetPos[4];		/* x,y,z,scale */
	
	private: float		ActiveFillCurrOpa;
	private: float		ActiveFillTargetOpa;
	private: float		TracingPathWidth;
	private: float		TracingCurrOpa;
	private: float		TracingTargetOpa;

};

void StringToPath(Path *p, string &s);

#endif
