/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 *
 ****************************************************************************/
 
#include "Elements.hpp"
#include <sstream>
#include <iostream>

using namespace std;

Word::Word()
{
	this->Time = 0;
	this->Score = 0;
	this->Generation = 0;
}

Word::Word(const Word &o)
{
	*this = o;
}

string Word::toString()
{
	stringstream buf;
	
	string ret;

	buf << "{";
	buf << this->Time;
	buf << ",";
	buf << this->Spell;
	buf << ",";
	buf << this->Score;
	buf << "}";
	
	buf >> ret;
	
	return ret;
}

int Word::parse(const string &s)
{
	stringstream buf;
	
	int p1, p2;
	
	p1 = s.find(",", 0);
	
	if (p1 == string::npos) return -1;
	
	p2 = s.find(",", p1 + 1);
	
	if (p2 == string::npos) return -1;
	
	buf << s.substr(1, p1 - 1);
	buf >> this->Time;
	
	this->Spell = s.substr(p1 + 1, p2 - p1 - 1);
	
	buf.str("");
	buf << s.substr(p2 + 1, s.length() - p2 - 2);
	buf >> this->Score;
		
	return 0;
}

Word &Word::operator =(const Word &dest)
{
	this->Time = dest.Time;
	this->Score = dest.Score;
	this->Spell = dest.Spell;
	this->Generation = dest.Generation;
	
	return *this;
}

int Word::operator ==(const Word &testee)
{
	if (abs(this->Time - testee.Time) > 20) return 0;
	if (this->Score != testee.Score) return 0;
	if (this->Spell != testee.Spell) return 0;
	
	return 1;
}

string Path::toString()
{
	// <path size=???>{...}{...}</path>
	
	stringstream buf;
	string ret;
	
	char s = 32;
	
	buf << "<path";
	buf << s;
	buf << "size=";
	buf << Words.size();
	buf << ">";
	
	vector<Word *>::iterator iter;
	
	for (iter = Words.begin(); iter != Words.end(); iter ++) {
		buf << (*iter)->toString();
	}
	
	buf << "</path>";
		
	//buf >> ret;
	
	return buf.str();
}

int Path::parse(const string &s)
{
	stringstream buf;
	
	if (s.substr(0, 11) != "<path size=") return -1;
	
	int p1 = s.find(">", 11);
	
	if (p1 == string::npos) return -1;
	
	string sl = s.substr(11, p1 - 11);
	
	int len;
	
	buf << sl;
	buf >> len;
	buf.str("");
	
	if (len < 0) return -1;
	
	clear();
	
	int wbegin;
	int wend = 12;
	
	for (int i = 0; i < len; i ++) {
		// Extract each {word...} and create Word objects
		wbegin = wend + 1;
		
		wend = s.find("}", wbegin);
		
		if (wend == string::npos) return -1;
		
		Word *w = new Word();
		
		string sword = s.substr(wbegin, wend - wbegin + 1);

		if (w->parse(sword) != 0) {
			delete w;
			return -1;
		}
		
		Words.push_back(w);
	}
		
	return 0;
}

int Path::size() const
{
	return Words.size();
}

Word Path::getWord(int n) const
{
	Word w(*(Words[n]));
	
	return w;
}

void Path::addWord(Word &o)
{
	Word *w = new Word(o);
	Words.push_back(w);
}

Path::~Path()
{
	clear();
}

void Path::clear()
{
	vector<Word *>::iterator iter;
	
	for (iter = Words.begin(); iter != Words.end(); iter ++) {
		delete (*iter);
	}
	
	Words.clear();
}
