/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 *
 ****************************************************************************/

#include <stdlib.h> 
#include <string>
#include <vector>

using namespace std;

#ifndef _SVGL_ELEMENTS_
#define _SVGL_ELEMENTS_

/* Stores a single word, with time and score info */
struct Word {
	public: long	Time;
	public: string	Spell;
	public: float	Score;
	
	public: int	Generation;
	
	public: Word();
	public: Word(const Word &o);
	public: string toString();
	public: int parse(const string &s);

	public: Word &operator =(const Word &dest);
	public: int operator ==(const Word &testee);
};

/* Stores a path consists several words */
struct Path {
	public: string toString();
	public: int parse(const string &s);
	public: int size() const;
	public: Word getWord(int n) const;
	public: void addWord(Word &o);
	
	public: ~Path();
	
	private: void clear();

	private: vector<Word *>	Words;
};

#endif
