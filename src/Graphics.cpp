/*****************************************************************************
 *
 * Dytraed - Dynamic Treancrip Editor/Sphinx Visualizer GL
 *
 * � 2007 Bo Zhang, Engineering Dept, University of Waikato, New Zealand
 *        Saturnino Luz, Trinity College Dublin, Ireland
 *
 *
 ****************************************************************************/

#include "Graphics.hpp"

void Graphics::setViewPosition(GLfloat x, GLfloat y, GLfloat z)
{
	ViewPosition[0] = x;
	ViewPosition[1] = y;
	ViewPosition[2] = z;
}

void Graphics::setViewTarget(GLfloat x, GLfloat y, GLfloat z)
{
	ViewTarget[0] = x;
	ViewTarget[1] = y;
	ViewTarget[2] = z;
}

void Graphics::setColor(GLfloat c[4])
{
	memcpy(DrawColor, c, sizeof (GLfloat) * 4);
}

void Graphics::drawText(GLfloat x, GLfloat y, GLfloat z, const string &s, float size)
{
	glColor4fv(DrawColor);
	glPushMatrix();
	
	/* Set up transformations */
	glTranslatef(x, y, z);
	glScalef(size, size, size);
	
	/* Draw item */
	renderStrokeString(GLUT_STROKE_ROMAN, s.c_str());
	
	glPopMatrix();
}


void Graphics::drawLine(GLfloat from[3], GLfloat to[3], float width)
{
	glColor4fv(DrawColor);
	
	glBegin(GL_LINES);
	
	glVertex3fv(from);
	glVertex3fv(to);
	
	glEnd();
}

void Graphics::drawPlaneXY(GLfloat z, GLfloat size[2])
{
	glColor4fv(DrawColor);
	
	glBegin(GL_QUADS);
	
	glVertex3f(-size[0], -size[1], z);
	glVertex3f(-size[0], size[1], z);
	glVertex3f(size[0], size[1], z);
	glVertex3f(size[0], -size[1], z);
	
	glEnd();
}

void Graphics::drawQuad(GLfloat *v1, GLfloat *v2, GLfloat *v3, GLfloat *v4)
{
	glColor4fv(DrawColor);
	
	glBegin(GL_QUADS);
	
	glVertex3fv(v1);
	glVertex3fv(v2);
	glVertex3fv(v3);
	glVertex3fv(v4);
	
	glEnd();
}


void Graphics::resize(int w, int h)
{
	ViewSize[0] = w;
	ViewSize[1] = h;
	
	glViewport(0, 0, w, h);
	
	glGetIntegerv(GL_VIEWPORT, Viewport);
	
	setUpTransformation();
}

Graphics::Graphics(string title, int w, int h)
{
	char *argv = "";
	int argc = 1;
	
	/* Initialize GLUT state */
	glutInit(&argc, &argv);

	/* Select type of Display mode: Double buffer & RGBA color */ 
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);

	glutInitWindowSize(w, h);
	glutInitWindowPosition(0, 0);

	/* Open a window */
	glutCreateWindow(title.c_str());

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearDepth(1.0f);
	
	DrawColor[0] = 0;
	DrawColor[1] = 0;
	DrawColor[2] = 0;
	DrawColor[3] = 0;
	
	CurrViewPosition[0] = 0;
	CurrViewPosition[1] = 0;
	CurrViewPosition[2] = -10;
	
	CurrViewTarget[0] = 0;
	CurrViewTarget[1] = 0;
	CurrViewTarget[2] = 0;
	
	resize(w, h);
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_LINE_SMOOTH);
	glLineWidth(2.0);
}

Graphics::~Graphics()
{
	/* Clean ups !? */
}

/* Update camera animation */
void Graphics::updateFrame(float time)
{
	ValueApproachExp(CurrViewPosition[0], ViewPosition[0], time, 20);
	ValueApproachExp(CurrViewPosition[1], ViewPosition[1], time, 20);
	ValueApproachExp(CurrViewPosition[2], ViewPosition[2], time, 20);
	
	ValueApproachExp(CurrViewTarget[0], ViewTarget[0], time, 40);
	ValueApproachExp(CurrViewTarget[1], ViewTarget[1], time, 40);
	ValueApproachExp(CurrViewTarget[2], ViewTarget[2], time, 40);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(CurrViewPosition[0], CurrViewPosition[1], CurrViewPosition[2],
		CurrViewTarget[0], CurrViewTarget[1], CurrViewTarget[2],
		0, 1, 0);
	glGetDoublev(GL_MODELVIEW_MATRIX, ViewMatrix);
}

void Graphics::setUpTransformation()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, (float)ViewSize[0] / (float)ViewSize[1], 1, 1000);	
	glGetDoublev(GL_PROJECTION_MATRIX, ProjectionMatrix);
	
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(CurrViewPosition[0], CurrViewPosition[1], CurrViewPosition[2],
		CurrViewTarget[0], CurrViewTarget[1], CurrViewTarget[2],
		0, 1, 0);
	glGetDoublev(GL_MODELVIEW_MATRIX, ViewMatrix);
}

void Graphics::getMouseRay(int winX, int winY, GLfloat* vp1, GLfloat* vp2)
{
	// Flip mouse Y coordinate as in screen space Down is positive
	winY = ViewSize[1] - winY;

	double vstart[3], vend[3];
	
	gluUnProject(winX, winY, 0.0, ViewMatrix, ProjectionMatrix, Viewport, &vstart[0], &vstart[1], &vstart[2]);
	gluUnProject(winX, winY, 1.0, ViewMatrix, ProjectionMatrix, Viewport, &vend[0], &vend[1], &vend[2]);

	vp1[0] = (float)vstart[0];
	vp1[1] = (float)vstart[1];
	vp1[2] = (float)vstart[2];
	
	vp2[0] = (float)vend[0];
	vp2[1] = (float)vend[1];
	vp2[2] = (float)vend[2];
}


void Graphics::renderBitmapString(void *font, const char *str)
{
	int len = (int) strlen(str);
	for (int i = 0; i < len; i++) {
		glutBitmapCharacter(font, str[i]);
	}
}

void Graphics::renderStrokeString(void *font, const char *str)
{
	int len = (int) strlen(str);
	for (int i = 0; i < len; i++) {
		glutStrokeCharacter(font, str[i]);
	}
}


float measureText(const string &s, float size)
{
	float w = 0.0f;
	const char *str = s.c_str();
	
	int len = (int) strlen(str);
	for (int i = 0; i < len; i++) {
		w += glutStrokeWidth(GLUT_STROKE_ROMAN, str[i]) * size;
	}
	
	return w;
}

void ValueApproachExpPrecise(float &curr, float target, float time, float speed)
{
	float diff = curr - target;
	float step = (curr - target) / 1.5f;
	
	if (step < 0) step = -step;
	
	step += speed;
	
	if (diff < 0) {
		curr += time * step;
		if (curr - target >= 0) {
			curr = target;
		}
	} else if (diff > 0) {
		curr -= time * step;
		if (curr - target <= 0) {
			curr = target;
		}
	}
}

void ValueApproachExp(float &curr, float target, float time, float speed)
{
	float diff = curr - target;
	float step = (curr - target) / 1.5f;
	
	if (step < 0) step = -step;
	
	step += speed;
	
	if (diff < 0.1f) {
		curr += time * step;
		if (curr - target >= -0.1f) {
			curr = target;
		}
	} else if (diff > 0.1f) {
		curr -= time * step;
		if (curr - target <= 0.1f) {
			curr = target;
		}
	}
}

void ValueApproachLinear(float &curr, float target, float time, float speed)
{
	float diff = curr - target;
	
	if (diff < -0.1f) {
		curr += time * speed;
		if (curr - target >= -0.1f) {
			curr = target;
		}
	} 
  else if (diff > 0.1f) {
		curr -= time * speed;
		if (curr - target <= 0.1f) {
			curr = target;
		}
	}
}

float Lerp(float a, float b, float factor)
{
	return (a - (a - b) * factor);
}

bool rayTriangleIntersect(GLfloat *p1, GLfloat *p2, GLfloat *v0, GLfloat *v1, GLfloat *v2, GLfloat *retpos)
{
	GLfloat D[3], e1[3], e2[3], P[3], T[3], Q[3];
	double det, inv_det, u, v;

	// calculate length of vector
	// D = p2 - p1
	D[0] = p2[0] - p1[0];
	D[1] = p2[1] - p1[1];
	D[2] = p2[2] - p1[2];

	// calculate triangle edges
	// e1 = v1 - v0
	e1[0] = v1[0] - v0[0];
	e1[1] = v1[1] - v0[1];
	e1[2] = v1[2] - v0[2];

	// e2 = v2 - v0
	e2[0] = v2[0] - v0[0];
	e2[1] = v2[1] - v0[1];
	e2[2] = v2[2] - v0[2];

	// P = D x e2
	P[0] = D[1] * e2[2] - D[2] * e2[1];
	P[1] = D[2] * e2[0] - D[0] * e2[2];
	P[2] = D[0] * e2[1] - D[1] * e2[0];
	
	det = ((double) e1[0] * (double) P[0]) + 
			((double) e1[1] * (double) P[1]) + 
			((double) e1[2] * (double) P[2]);

	// parallel ray-triangles do not intersect.
	if (det == 0.0) return false;

	inv_det = 1.0 / det;
	
	// T = p1 - v0
	T[0] = p1[0] - v0[0];
	T[1] = p1[1] - v0[1];
	T[2] = p1[2] - v0[2];

	u = (((double) T[0] * (double) P[0]) + 
			((double) T[1] * (double) P[1]) + 
			((double) T[2] * (double) P[2])) * inv_det;

	if (u < 0.0 || u > 1.0) return false; // u range error

	// Q = T x e1
	Q[0] = T[1] * e1[2] - T[2] * e1[1];
	Q[1] = T[2] * e1[0] - T[0] * e1[2];
	Q[2] = T[0] * e1[1] - T[1] * e1[0];
	

	v = (((double) D[0] * (double) Q[0]) + 
			((double) D[1] * (double) Q[1]) + 
			((double) D[2] * (double) Q[2])) * inv_det;

	if (v < 0.0 || u + v > 1.0)
	return false; // v range error


	*retpos = (float)((((double) e2[0] * (double) Q[0]) + 
			((double) e2[1] * (double) Q[1]) + 
			((double) e2[2] * (double) Q[2]))*inv_det);
	return true;
}
