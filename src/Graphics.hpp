/*****************************************************************************
 *
 * Dytraed - Dynamic Treancrip Editor/Sphinx Visualizer GL
 *
 * � 2007 Bo Zhang, Engineering Dept, University of Waikato, New Zealand
 *        Saturnino Luz, Trinity College Dublin, Ireland
 *
 *
 ****************************************************************************/

#include <stdlib.h>
#include <cstring>

#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <string>

using namespace std;

#ifndef _SVGL_GRAPHICS_
#define _SVGL_GRAPHICS_

class Graphics {
	
	public: void setViewPosition(GLfloat x, GLfloat y, GLfloat z);
	public: void setViewTarget(GLfloat x, GLfloat y, GLfloat z);
	
	public: void setColor(GLfloat c[4]);
	public: void drawText(GLfloat x, GLfloat y, GLfloat z, const string &s, float size);
	public: void drawLine(GLfloat from[3], GLfloat to[3], float width);
	public: void drawPlaneXY(GLfloat z, GLfloat size[2]);
	public: void drawQuad(GLfloat *v1, GLfloat *v2, GLfloat *v3, GLfloat *v4);

	public: void resize(int w, int h);
	
	public: void updateFrame(float time);
	
	public: void getMouseRay(int winX, int winY, GLfloat* vstart, GLfloat* vend);
	
	public: Graphics(string title, int w, int h);
	public: ~Graphics();
	
	private: void setUpTransformation();
	private: void renderBitmapString(void *font, const char *str);
	private: void renderStrokeString(void *font, const char *str);

	private: GLdouble	ViewMatrix[16];
	private: GLdouble	ProjectionMatrix[16];
	private: GLint		Viewport[4];
	
	private: GLfloat	CurrViewPosition[3];
	private: GLfloat	CurrViewTarget[3];
	
	private: GLfloat	ViewPosition[3];
	private: GLfloat	ViewTarget[3];
	private: GLfloat	DrawColor[4];
	private: GLint		ViewSize[2];
};

float measureText(const string &s, float size);
void ValueApproachLinear(float &curr, float target, float time, float speed);
void ValueApproachExp(float &curr, float target, float time, float speed);
void ValueApproachExpPrecise(float &curr, float target, float time, float speed);
float Lerp(float a, float b, float factor);
bool rayTriangleIntersect(GLfloat *p1, GLfloat *p2, GLfloat *v0, GLfloat *v1, GLfloat *v2, GLfloat *retpos);

#endif
