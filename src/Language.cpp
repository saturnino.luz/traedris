#include <sstream>
#include <fstream>
#include "Language.hpp"

using namespace std;

static char *SMALL_WORDS[] = {"a", "an", "am", "at", "are", "and", "the", "be", "by", "do", "I", "is", "if", "it", "in", "on", "of", "or", "so", NULL};

static bool IsSmallWord(string &s)
{
	for (int i = 0; SMALL_WORDS[i] != NULL; i ++) {
		if (s == SMALL_WORDS[i]) return true;
	}
	
	return false;
}

void Sentence::clear()
{
	Score = 0;
	
	vector<Word *>::iterator iter;
	for (iter = Words.begin(); iter != Words.end(); iter ++) {
		delete (*iter);
	}
	Words.clear();
}

int Sentence::size()
{
	return Words.size();
}

Word *Sentence::getWord(int n)
{
	if (n >= 0 && Words.size() > n) {
		return Words[n];
	} else {
		return NULL;
	}
}

/* Parse a sentence in a string, whitespaces are treated as
 * markers between words
 */
void Sentence::parse(string &s)
{
	stringstream ss;
	string sw;
	
	int pos;
	
	clear();
	
	ss << s;	

	int p = ss.tellg();
	
	while (!ss.eof()) {
		ss >> sw;
		
		if (ss.tellg() < 0) break;
		
		Word *w = new Word();
		
		w->Time = pos ++;
		w->Spell = sw;
		Words.push_back(w);		
	}	
}

/* Scan through sentence to find number of matching words.
 * ordered == true: words must appear in given order
 * Naive implemetation: O(N^2) complexity */
int Sentence::wordMatch(Sentence &s, bool ordered, int &lastwordpos)
{
	int n = 0, len = Words.size();
	int start = 0, i;
	
	vector<Word *>::iterator iter;
	for (iter = s.Words.begin(); iter != s.Words.end(); iter ++) {
		Word *tgt = (*iter);
		
		for (i = start; i < len; i ++)
		{
			if (tgt->Spell == Words[i]->Spell) {
				lastwordpos = i;
				if (ordered) start = i + 1;
				
				if (!::IsSmallWord(tgt->Spell)) {
					n += 2;
				} else {
					n ++;
				}
				break;
			}
		}
	}
	
	return n;
}

Sentence::Sentence() {}

Sentence::Sentence(string &s)
{
	parse(s);
}

Sentence::~Sentence()
{
	clear();
}

/* No one needs to know how this works, it basically moves ownership of
 * words in a sentence to another, as used for quick copying and object
 * then discard the former object */
void Sentence::liveMigrate(vector<Word *> &from)
{
	clear();	
	vector<Word *>::iterator iter;
	for (iter = from.begin(); iter != from.end(); iter ++) {
		Words.push_back(*iter);
	}
	from.clear();
}

void Sentence::copyFromAndEmpty(Sentence &s)
{
	liveMigrate(s.Words);
	Score = s.Score;
}

/* Whole sentence copy */
Sentence &Sentence::operator =(Sentence &dest)
{
	clear();
	
	Score = dest.Score;
	
	vector<Word *>::iterator iter;
	
	for (iter = dest.Words.begin(); iter != dest.Words.end(); iter ++) {
		Word *nw = new Word(*(*iter));
		Words.push_back(nw);
	}
	
	return *this;
}

/* Note: Score is not being compared, do it yourself! */
int Sentence::operator ==(const Sentence &s)
{
	if (s.Words.size() != Words.size()) return 0;
	
	for (int i = 0; i < Words.size(); i ++) {
		if (Words[i] != s.Words[i]) return 0;
	}
	
	return 1;
}

/****************************************************************************/

void SimpleWordAutoFiller::clear()
{
	vector<Sentence *>::iterator iter;
	for (iter = SentList.begin(); iter != SentList.end(); iter ++) {
		delete (*iter);
	}
	SentList.clear();
	
	SuggCount = 0;
}

/* Sentence will be checked against duplication.
 * Warning: destroys passed in sentence after add! */
int SimpleWordAutoFiller::addSentence(Sentence &s)
{
	/* Empty sentence? */
	if (s.size() == 0) return 0;
	
	vector<Sentence *>::iterator iter;
	for (iter = SentList.begin(); iter != SentList.end(); iter ++) {
		/* Found a duplication, don't add */
		if (s == *(*iter)) return 0;
	}
	
	Sentence *newsent = new Sentence();
	newsent->copyFromAndEmpty(s);
	SentList.push_back(newsent);
}

int SimpleWordAutoFiller::loadPlainTextFile(string filename)
{
	ifstream f(filename.c_str());
	string str;
	Sentence worker;
	
	if (f.fail()) return -1;
	
	while (!f.eof()) {
		getline(f, str);
		worker.parse(str);
		addSentence(worker);
	}
	
	f.close();

	return 0;
}

int SimpleWordAutoFiller::findSuggestions(Sentence &current, int maxItems)
{
	if (maxItems < 1) return -1;
	
	SuggCount = 0;
	
	Word *lastWord = current.getWord(current.size() - 1);
	
	if (lastWord == NULL) return -1;
	
	int i, j, pos, taillen;
	bool used;

	for (j = 0; j < SentList.size(); j ++) {
		pos = 0;
		
		/* We are promoted by matching words */		
		SentList[j]->Score = SentList[j]->wordMatch(current, false, pos) +
					SentList[j]->wordMatch(current, true, pos) * 2;
					
		taillen = SentList[j]->size() - pos;
		if (taillen > 4) taillen = 4;
		
		/* We are promoted by offering short sentence */
		SentList[j]->Score += (4 - taillen) * (4 - taillen) * 10;
		
		/* We are punished by offering duplicate information */
		SentList[j]->Score -= suggMatch(j);
		
		used = false;
		
		/* Last match can't be the end of the sentence */
		if (pos == SentList[j]->size() - 1) continue;
		
		/* Strictly match the last word so we can extent current sentence */
		if (lastWord->Spell != SentList[j]->getWord(pos)->Spell) continue;
		
		for (i = 0; i < SuggCount; i ++) {
			if (SentList[j]->Score > SentList[SuggList[i]]->Score) {
				used = true;
				suggInsert(i, j, pos);
				break;
			}
		}
		
		if (!used && SuggCount < SVGL_SENTENCE_SUGGESTION_MAX) {
			suggInsert(SuggCount, j, pos);
		}
	}
	
	return SuggCount;
}

void SimpleWordAutoFiller::suggInsert(int listid, int sentid, int beginpos)
{
	int i, j;
	
	int taillen = SentList[sentid]->size() - beginpos;
	bool bdup;
	
	/* Check for duplication */
	for (i = 0; i < SuggCount; i ++) {
		int tl = SentList[SuggList[i]]->size() - SuggBegin[i];
		
		/* Potential duplication, check in detail... */
		if (tl == taillen) {
			bdup = true;
			for (j = 0; j < taillen; j ++) {
				if (SentList[sentid]->getWord(beginpos + j)->Spell !=
					SentList[SuggList[i]]->getWord(SuggBegin[i] + j)->Spell) {
					bdup = false;
					break;	
				}
			}
			if (bdup) return;
		}
	}

	if (SuggCount <= listid) {
		/* We had too much sentences, simply exit */
		if (SuggCount >= SVGL_SENTENCE_SUGGESTION_MAX - 1) return;
		
		/* Append to last */
		SuggList[SuggCount] = sentid;
		SuggBegin[SuggCount] = beginpos;
		
		++ SuggCount;
	} else {
		/* Inserion in the middle */
		if (SuggCount >= SVGL_SENTENCE_SUGGESTION_MAX) SuggCount --;
		
		for (i = SuggCount; i > listid; i --) {
			SuggList[i] = SuggList[i - 1];
			SuggBegin[i] = SuggBegin[i - 1];
		}
		SuggList[listid] = sentid;
		SuggBegin[listid] = beginpos;
		
		if (SuggCount < SVGL_SENTENCE_SUGGESTION_MAX) SuggCount ++;
	}
}

/* Match a sentence against suggestions, return score of highest match */
int SimpleWordAutoFiller::suggMatch(int sentid)
{
	int ms = 0, tmp;
	int pos;
	
	for (int i = 0; i < SuggCount; i ++) {
		if (sentid != SuggList[i]) {
			tmp = SentList[SuggList[i]]->wordMatch(*SentList[sentid], false, pos);
			tmp += SentList[SuggList[i]]->Score / 2;
			if (ms < tmp) ms = tmp;
		}
	}
	
	return ms;
}

Sentence *SimpleWordAutoFiller::getSuggestion(int n, int &startpos)
{
	if (SuggCount > n) {
		startpos = SuggBegin[n];
		return SentList[SuggList[n]];
	} else {
		return NULL;
	}
}

SimpleWordAutoFiller::SimpleWordAutoFiller()
{
	SuggCount = 0;
}

SimpleWordAutoFiller::~SimpleWordAutoFiller()
{
	clear();
}
