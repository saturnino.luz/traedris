#include <iostream>
#include <fstream>
#include "Elements.hpp"

using namespace std;

#ifndef _SVGL_LANGUAGE_
#define _SVGL_LANGUAGE_

#define SVGL_SENTENCE_SUGGESTION_MAX	16

class Sentence {
	public: void clear();
	public: int size();
	public: Word *getWord(int n);
	public: void parse(string &s);
	public: int wordMatch(Sentence &s, bool ordered, int &lastwordpos);
	
	/* Used for suggestion score sorting only */
	public: int Score;
	
	public: Sentence();
	public: Sentence(string &s);
	public: ~Sentence();
	
	public: Sentence &operator =(Sentence &dest);
	public: int operator ==(const Sentence &s);
	
	public: void copyFromAndEmpty(Sentence &s);
	
	private: void liveMigrate(vector<Word *> &from);
	private: vector<Word *>		Words;
};

class AutoFiller {
	public: virtual int addSentence(Sentence &s) = 0;
	public: virtual int findSuggestions(Sentence &current, int maxItems) = 0;
	public: virtual Sentence *getSuggestion(int n, int &startpos) = 0;
};

class SimpleWordAutoFiller : public AutoFiller {
	public: int addSentence(Sentence &s);
	public: int findSuggestions(Sentence &current, int maxItems);
	public: Sentence *getSuggestion(int n, int &startpos);

	public: void clear();
	public: int loadPlainTextFile(string filename);	

	public: SimpleWordAutoFiller();
	public: ~SimpleWordAutoFiller();
	
	private: void suggInsert(int listid, int sentid, int beginpos);
	private: int  suggMatch(int sentid);
	private: int			SuggCount;
	private: int			SuggBegin[SVGL_SENTENCE_SUGGESTION_MAX];
	private: int			SuggList[SVGL_SENTENCE_SUGGESTION_MAX];
	private: vector<Sentence *>	SentList;
};

#endif
