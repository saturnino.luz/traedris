/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 *
 ****************************************************************************/

#include <iostream>
#include <fstream>
#include "ReadWrite.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#define SVGL_NETWORK_PORT	4301
#define SVGL_NETWORK_BUF_SIZE	16384

using namespace std;

int NetworkReader::open(string &path)
{
	if (opened) return -1;
	
	cout << "Opening network address " << path << " for input ...";
	
	struct sockaddr_in remote_addr;
	
	int addr_len, numbytes;
	int insize, result;
	
	bzero(&remote_addr,sizeof(remote_addr));
	/* host byte order */
	remote_addr.sin_family = AF_INET;
	/* short, network byte order */
	remote_addr.sin_port = htons(SVGL_NETWORK_PORT);
	/* automatically fill with my IP */
	inet_aton(path.c_str(), &remote_addr.sin_addr);
	
	if ((sockfd = ::socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		return -1;
	}
	
	if (::connect(sockfd, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr)) == -1)
	{
		return -1;
	}
	
	buffer = new char[SVGL_NETWORK_BUF_SIZE];
	
	cout << " done." << endl;
	
	opened = 1;
	
	return 0;
}

int NetworkReader::close()
{
	if (!opened) return -1;
	
	//::close(sockfd);

	delete[] buffer;
	
	opened = 0;
	
	return 0;
}

Snapshot *NetworkReader::read()
{
	if (!opened) return NULL;
	
	int len = ::recv(sockfd, buffer, SVGL_NETWORK_BUF_SIZE, 0);

  //cout << "SocketFD: " << sockfd <<"\n";

	if (len == 0) return NULL;
	
	string line(buffer);
	
	Snapshot *ss = new Snapshot();
	
	int res = ss->parse(line);
	
	if (res < 0) {
		cout << "Error: " << res << endl;
		delete ss;
		return NULL;
	}

	return ss;
}

int NetworkReader::sendctlmsg(string msg)
{
  if (!opened) return -1;

  const char *m = msg.c_str();

  int r = ::send(sockfd, m, msg.length(), 0); 

  if (r == -1)
    cout << "NetworkReader: error sending '" << msg << "': " << strerror(errno) <<"\n";

  return r;

}


int FileReader::open(string &path)
{
	if (opened) return -1;
	
	cout << "Opening file " << path << " for input ...";
	
	fin.open(path.c_str());
	
	opened = (fin.fail() ? 0 : 1);
	
	if (opened) cout << " done." << endl;
	else cout << " failed." << endl;

	return (!opened);
}

int FileReader::close()
{
	if (!opened) return -1;
	
	opened = 0;
	fin.close();
	
	return 0;
}

Snapshot *FileReader::read()
{
	if (!opened) return NULL;
	
	/* TODO: Add timing control function to return lines on designated time */
	if (fin.eof()) return NULL;
	
	string line;
	
	getline(fin, line);

	Snapshot *ss = new Snapshot();
	
	if (ss->parse(line) < 0) {
		delete ss;
		return NULL;
	}
	

	return ss;
 }

int FileReader::sendctlmsg(string msg){
  return -1;
}

int FileWriter::open(string &path)
{
	if (opened) return -1;
	
	cout << "Opening file " << path << " for output ...";
	
	fout.open(path.c_str());
	
	opened = (fout.fail() ? 0 : 1);
	
	if (opened) cout << " done." << endl;
	else cout << " failed." << endl;

	return (!opened);
}

int FileWriter::close()
{
	if (!opened) return -1;
	
	opened = 0;
	fout.close();
	
	return 0;
}

int FileWriter::write(Snapshot *data)
{
	if (!opened) return -1;
	
	fout << data->toString() << endl;
	
	return 0;
}
