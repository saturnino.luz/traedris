/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 * S Luz, Dept of Computer Science. TCD, 2007/8
 *
 ****************************************************************************/
#include <stdlib.h>
#include <cstring>
 
#include <vector>
#include <fstream>
#include "Elements.hpp"
#include "Snapshot.hpp"

#ifndef _SVGL_READWRITE_
#define _SVGL_READWRITE_

#define SVGL_MSG_HI "HI\n"
#define SVGL_MSG_BYE  "BYE\n"
#define SVGL_MSG_PAUSE "PAUSE\n"
#define SVGL_MSG_CONT "CONTINUE\n"

using namespace std;

class SnapshotIO {
public: SnapshotIO(): opened(0) {};
public: virtual int isopen() const { return opened; }
public: virtual int open(string &path) = 0;
public: virtual int close() = 0;
protected: int	opened;
};


class SnapshotReader : public SnapshotIO 
{
public: virtual Snapshot *read() = 0;
public: virtual int sendctlmsg(string msg) = 0;
};

class SnapshotWriter : public SnapshotIO {
public: virtual int write(Snapshot *data) = 0;
};

class NetworkReader : public SnapshotReader {
public: bool isopen();
public: int open(string &path);
public: int close();
public: Snapshot *read();
public: int sendctlmsg(string msg);

public: virtual int isopen() const { return opened; }
	
private: int opened;
private: char *buffer;
private: int sockfd;
};

class FileReader : public SnapshotReader {
public: bool isopen();
public: int open(string &path);
public: int close();
public: Snapshot *read();
public: int sendctlmsg(string msg);
public: virtual int isopen() const { return opened; }
	
private: int opened;
private: ifstream fin;
};

class FileWriter : public SnapshotWriter {
public: bool isopen();
public: int open(string &path);
public: int close();
public: int write(Snapshot *data);
public: int isopen() const { return opened; }
	
private: int opened;
private: ofstream fout;
};

#endif
