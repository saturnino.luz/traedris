/*****************************************************************************
 *
 * TRAEDRIS - A transcription editor game
 *
 * � 2007 Bo Zhang, Engineering Dept, University of Waikato, New Zealand
 *   2009 
 *   2010 Saturnino Luz, Trinity College Dublin, Ireland
 *
 *
 ****************************************************************************/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include "SVGL.hpp"
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <pthread.h>
#include <time.h>
#include "Language.hpp"
#include "Revision.hpp"

using namespace std;

SVGL *Application;
static void GLUTReshape(int w, int h);
static void GLUTProcTimer(int time);
static void GLUTProcDisplay(void);
static void *GLUTProcThread(void *);
static void GLUTProcKeyboard(unsigned char key, int x, int y);
static void GLUTProcMouse(int button, int state, int x, int y);
pthread_t GLUTProcThreadID;

#define RENDER_SPEED_INTERVAL	33		/* Milli-sec between render frame 16 = 60 fps*/

static float PlaySpeeds[6] = {0.25f, 0.33f, 0.5f, 1.0f, 2.0f, 3.0f};

/* Main entrance point for the application */
void SVGL::AppEntry(vector<string *> args)
{
  int i = 0;
	
  pauseFlashCounter = 0;
  recordFlashCounter = 0;
  isPaused = true;
  isRunning = true;
  isInputFinished = false;
  playSpeed = 3;
	
  pthread_mutex_init(&DataLock, NULL);
	
  if (args[i]->compare("-n") == 0) {
    /* Open network source */
    reader = new NetworkReader();
    Input = *args[++ i];
    reader->open(*args[i]);
    isLiveFeed = true;
  } else {
    /* Open disk source */
    reader = new FileReader();
    Input = *args[i];
    reader->open(*args[i]);
    isLiveFeed = false;
  }
	
  if (++ i < args.size()) {
    /* Open disk for output recording */
    Output = *args[i];
    writer = new FileWriter();
    writer->open(*args[i]);
  } else {
    /* No output */
    writer = NULL;
    Output = "(none)";
  }
	
  SimpleWordAutoFiller *sf = new SimpleWordAutoFiller();
  sf->loadPlainTextFile("autofill");
	
  wordfiller = sf;
	 
  view = new SVView();
	
  view->setAutoFiller(wordfiller);
	
  initWindow();

  /* Register Callback Functions */
  glutReshapeFunc(GLUTReshape);
  glutDisplayFunc(GLUTProcDisplay);
  glutKeyboardFunc(GLUTProcKeyboard);
  glutMouseFunc(GLUTProcMouse);
  glutTimerFunc(RENDER_SPEED_INTERVAL, GLUTProcTimer, RENDER_SPEED_INTERVAL);

  pthread_create(&GLUTProcThreadID, NULL, GLUTProcThread, (void *) NULL);

  /* Start Event Processing Engine */
  glutMainLoop();
	
  reader->close();
  cout << "Reader closed\n";
	
  SVView *viewstub = view;
  view = NULL;

  pthread_join(GLUTProcThreadID, NULL);
		
  delete viewstub;
	
  termWindow();
	
  delete reader;
	
  delete graphics;
  delete wordfiller;
	
  pthread_mutex_destroy(&DataLock);
	
  if (writer != NULL) {
    writer->close();
    delete writer;
  }
}

int SVGL::initWindow()
{
  graphics = new Graphics(string("TRAEDRIS v")+REVISION, 1024, 768);
	
  graphics->setViewPosition(20, 130, 320);
  graphics->setViewTarget(170, 0, 0);
	
  return 0;
}

void SVGL::reshape(int w, int h)
{
  if (graphics != NULL) {
    graphics->resize(w, h);
  }
}

void SVGL::updateFrame(float time)
{
  if (!isLiveFeed) time *= PlaySpeeds[playSpeed];
	
  graphics->updateFrame(time);
	
  if (view != NULL) {
    pthread_mutex_lock(&DataLock);
    view->updateFrame(time);
    pthread_mutex_unlock(&DataLock);
		
    glutPostRedisplay();
  }
}
#define KEY_ESC		27
#define KEY_SPACE	32

void SVGL::onKeyPress(unsigned char key, int x, int y)
{
  //cout << "Key: " << int(key) << endl;
  switch(key) {
  case KEY_SPACE:
    pauseFlashCounter = 16;
    if (!isPaused)
      reader->sendctlmsg(SVGL_MSG_PAUSE);
    else
      reader->sendctlmsg(SVGL_MSG_CONT);
    isPaused = !isPaused;
    break;
  case KEY_ESC:
    isRunning = false;
    reader->close();
    if (writer != NULL) {
      writer->close();
      delete writer;
      writer = NULL;
    }
    exit(0);
    break;
  case 43:
  case 61:
    if (playSpeed < 5) playSpeed ++;
    break;
  case 45:
    if (playSpeed > 0) playSpeed --;
    break;
  }
}

void SVGL::onMousePress(int button, int state, int x, int y)
{
  GLfloat p1[3], p2[3];
	
  // Button:
  // 	0 - left
  //	1 - mid
  //	2 - right
  //	3 - scroll up
  //	4 - scroll down

  if (button == 0 && state == 0) {
    graphics->getMouseRay(x, y, p1, p2);

    if (view != NULL) {
      view->onRayClick(p1, p2);
    }
  }
}

void SVGL::draw()
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /*glClearColor(1.0,1.0,1.0,0.0); */

  /* Draw 3D view */
  if (view != NULL) {
    view->draw(graphics);
  }
	
  /* Draw overlay texts */
  glPushMatrix();
  glLoadIdentity();
	
  static GLfloat white[4] = {1, 1, 1, 1};
  static GLfloat gray[4] = {0.3, 0.3, 0.3, 1};
  static GLfloat red[4] = {1, 0, 0, 1};
  static GLfloat yellow[4] = {1, 1, 0, 1};
  static GLfloat green[4] = {0, 1, 0, 1};
  static GLfloat blue[4] = {0, 0, 1, 1};
	
  char buf[8];
  string out = string("TRAEDRIS v") + REVISION;
	
  graphics->setColor(white);
  graphics->drawText(-27, 19, -50, out, 0.009f);
  
  out = " - Trinity College Dublin & The University of Waikato.";
  graphics->setColor(white);
  graphics->drawText(-18, 19, -50, out, 0.007f);

  out = "Mouse: click a word for actions";
  graphics->setColor(gray);
  graphics->drawText(-27, 17, -50, out, 0.008f);


  graphics->setColor(green);
  out = "Input stream: " + Input + (isLiveFeed ? " (Live-feed)" : "");
  graphics->drawText(-27, -19.5, -50, out, 0.007f);
	
  if (writer != NULL) {
    out = "Output stream: " + Output;
    graphics->drawText(-27, -18.5, -50, out, 0.007f);
		
		
		
    if (writer->isopen() && recordFlashCounter > 15) {
      float w = measureText(out, 0.009f);
      graphics->setColor(red);
      graphics->drawText(-27 + w, -19.5, -50, " Recording", 0.007f);
    }
  }

  if (isInputFinished && pauseFlashCounter > 15) {
    out = "Input read finished";
    graphics->setColor(red);
    graphics->drawText(-12, -11, -50, out, 0.02f);
  }
	
  if (++ pauseFlashCounter > 30) {
    pauseFlashCounter = 0;
  }
	
  if (++ recordFlashCounter > 30) {
    recordFlashCounter = 0;
  }

  if (isPaused && pauseFlashCounter > 15) {
    out = "Paused";
    graphics->setColor(yellow);
    graphics->drawText(-4.5, -16, -50, out, 0.02f);
  }
	
  if (!isLiveFeed) {
    stringstream spd;

    spd << "Speed: ";
    spd << PlaySpeeds[playSpeed];
    spd << "X";
    graphics->setColor(green);
    graphics->drawText(19.2, -19.4, -50, spd.str(), 0.007f);
  }
	
  glPopMatrix();
	
  glutSwapBuffers();
}

void SVGL::readThread()
{
  cout << "Start reading data ..." << endl;

  int result = SVVIEW_SNAPSHOT_OK;
  Snapshot *s = NULL;

  if (isLiveFeed)
    reader->sendctlmsg(SVGL_MSG_HI);
	
  while (isRunning) {
    if (isPaused) {
      usleep(100000);
      continue;
    }
		
    /* Read a Snapshot from input object */
		
    /* If viewer returned "try again", we will not fetch a new record */
    if (result != SVVIEW_SNAPSHOT_RETRY) {
      delete s;
      s = reader->read();
      /*
        if (isLiveFeed && s.getType() == LT_BEGIN)
        reader->sendctlmsg(SVGL_MSG_NEXT);
      */
      cout << "Read snapshot: " << s->toString() << endl;
      if (NULL == s) break;
		
      if (result == SVVIEW_SNAPSHOT_JUMP && s->getType() != LT_BEGIN) {
        /* Viewer explicitly tells us to jump over this snapshot entirely
         * so we will read until hit a LT_BEGIN page
         */
        if (writer != NULL) {
          writer->write(s);
        }
        continue;
      }
    } /* end if result != SVVIEW_SNAPSHOT_RETRY */
		
    if (!isLiveFeed) {
      if (result != SVVIEW_SNAPSHOT_RETRY) {
        usleep((long)(2000000 / PlaySpeeds[playSpeed]));
      } else {
        /* If viewer paused for action, we do short sleep to
         * preserve responsiveness */
        usleep(100000);
      }
    }

    while (isPaused && isRunning) {
      usleep(100000);
      continue;
    }

    if (view != NULL) {
      /* Deliver snapshot to view */
      pthread_mutex_lock(&DataLock);
      result = view->applySnapshot(s, graphics);
      pthread_mutex_unlock(&DataLock);
    }
			
    /* Deliver snapshot to output object if there is any
     * Check result to prevent duplicated writes */
    if (result != SVVIEW_SNAPSHOT_RETRY && writer != NULL) {
      writer->write(s);
    }
  } /* end while(running) */
	
  delete s;
	
	
  if (view != NULL) {
    /* Deliver snapshot to view */
    pthread_mutex_lock(&DataLock);
    view->newLayer();
    pthread_mutex_unlock(&DataLock);
  }
		
  isInputFinished = true;
	
  cout << "Data reading finished" << endl;

  if (writer != NULL) {
    writer->close();
  }
}

int SVGL::termWindow()
{
  cout << "Term" << endl;
  return 0;
}

static void GLUTReshape(int w, int h)
{
  Application->reshape(w, h);
}

static void GLUTProcTimer(int time)
{
  float intv = (float)time / 1000;
	
  Application->updateFrame(intv);
	
  glutTimerFunc(RENDER_SPEED_INTERVAL, GLUTProcTimer, RENDER_SPEED_INTERVAL);
}

static void GLUTProcDisplay(void)
{
  Application->draw();
}

static void *GLUTProcThread(void *)
{
  Application->readThread();
}

static void GLUTProcKeyboard(unsigned char key, int x, int y)
{
  Application->onKeyPress(key, x, y);
}

static void GLUTProcMouse(int button, int state, int x, int y)
{
  Application->onMousePress(button, state, x, y);
}

int main(int argc, char **argv)
{
  /*
    SimpleWordAutoFiller wf;
	
    wf.loadPlainTextFile("autofill");
	
    Sentence s;
    string w = argv[1];//"the green one on the table purple one";
    s.parse(w);
	
    int m = wf.findSuggestions(s, 8);
    int pos;
	
    cout << "Source sentence: " << w << endl;
    cout << "Number of suggestions: " << m << endl;
	
    for (int i = 0; i < m; i ++) {
    Sentence *ss = wf.getSuggestion(i, pos);
		
    cout << "Suggestion " << i << "(Score " << ss->Score << "): ";
		
    for (int j = pos; j < ss->size(); j ++) {
    cout << ss->getWord(j)->Spell << " ";
    }
		
    cout << endl;
    }

    return 0;
  */

  if (argc < 2) {
    cout << "Transcript Editor Game (v" << REVISION << ")" << endl;
    cout << "Usage: traedris [in_file | -n network_address] [out_file]" << endl;
    cout << "	in_file		: input file name" << endl;
    cout << "	network_address	: sphinx server address" << endl;
    cout << "	out_file	: save file name" << endl;
    return 0;
  }

  vector<string *> args;

  Application = new SVGL();
	
  for (int i = 1; i < argc; i ++) {
    args.push_back(new string(argv[i]));
  }

  Application->AppEntry(args);

  for (int i = 0; i < argc; i ++) {
    delete args[i];
  }
	
  args.clear();
	
  return 0;
}
