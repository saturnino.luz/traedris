/*****************************************************************************
 *
 * Dytraed - Dynamic Treancrip Editor/Sphinx Visualizer GL
 *
 * � 2007 Bo Zhang, Engineering Dept, University of Waikato, New Zealand
 *        Saturnino Luz, Trinity College Dublin, Ireland
 *
 *
 ****************************************************************************/

#include <vector>
#include <string>
#include <unistd.h>
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include "Elements.hpp"
#include "Snapshot.hpp"
#include "ReadWrite.hpp"
#include "Drawable.hpp"

using namespace std;

#ifndef _SVGL_MAINAPP_
#define _SVGL_MAINAPP_

class SVGL {
	public: void AppEntry(vector<string *> args);
	
	public: void onKeyPress(unsigned char key, int x, int y);
	public: void onMousePress(int button, int state, int x, int y);
	public: void reshape(int w, int h);
	public: void updateFrame(float time);
	public: void draw();
	public: void readThread();
	
	private: int initWindow();
	private: int termWindow();
	
	private: string Input;
	private: string Output;
	private: bool isRunning;
	private: bool isLiveFeed;
	private: bool isPaused;
	private: bool isInputFinished;
	private: int pauseFlashCounter;
	private: int recordFlashCounter;
	private: int playSpeed;
	
	private: AutoFiller *wordfiller;
	private: Graphics *graphics;
	private: SnapshotReader *reader;
	private: SnapshotWriter *writer;
	private: SVView *view;
	private: pthread_mutex_t DataLock;
};

#endif
