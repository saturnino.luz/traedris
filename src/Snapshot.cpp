/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 *
 ****************************************************************************/

#include "Snapshot.hpp"
#include <string>
#include <sstream>
#include <iostream>

using namespace std;

string Snapshot::toString()
{
	// <snapshot start=??? end=??? type=??? size=???><...><...></snapshot>
	
	stringstream buf;
	string ret;
	
	static char *snapshotheader[] = {"<snapshot",  "start=", "end=", "type=", "size="};
	char s = 32;
	
	buf << snapshotheader[0];
	buf << s;
	buf << snapshotheader[1];
	buf << TimeStart;
	buf << s;
	buf << snapshotheader[2];
	buf << TimeEnd;
	buf << s;
	buf << snapshotheader[3];
	buf << Type;
	buf << s;
	buf << snapshotheader[4];
	buf << size();
	buf << ">";
	
	vector<Path *>::iterator iter;
	
	for (iter = Paths.begin(); iter != Paths.end(); iter ++) {
		buf << (*iter)->toString();
	}
	
	buf << "</snapshot>";
	
	//buf >> ret;
	
	return buf.str();
}

int Snapshot::parse(string &s)
{
	int p1 = s.find(">", 0);
	
	if (p1 == string::npos) return -1;
	
	int mySize = parseHeader(s.substr(0, p1 + 1));
	
	if (mySize < 0) return -2;
	
	clear();
	
	int pStart;
	int pEnd = p1 - 6;
	
	for (int i = 0; i < mySize; i ++) {
		// Extract each <path>...</path> and create Path objects
		
		pStart = pEnd + 7;
		pEnd = s.find("</path>", pStart);
		
		Path *p = new Path();
		
		string spath = s.substr(pStart, pEnd - pStart + 7);
		
		if (p->parse(spath) != 0) {
			delete p;
			return -3;
		}
		
		Paths.push_back(p);
	}
	
	return 0;
}

void Snapshot::setTimeStart(float t)
{
	TimeStart = t;
}

void Snapshot::setTimeEnd(float t)
{
	TimeEnd = t;
}

float Snapshot::getTimeStart() const
{
	return TimeStart;
}

float Snapshot::getTimeEnd() const
{
	return TimeEnd;
}

LAYER_TYPE Snapshot::getType() const
{
	return Type;
}

int Snapshot::size() const
{
	return Paths.size();
}

const Path *Snapshot::getPath(int n)
{
	return (const Path *) Paths[n];
}

Snapshot::Snapshot()
{
	TimeStart = 0;
	TimeEnd = 0;
	Type = LT_NORMAL;
}

Snapshot::~Snapshot()
{
	clear();
}

void Snapshot::clear()
{
	vector<Path *>::iterator iter;
	
	for (iter = Paths.begin(); iter != Paths.end(); iter ++) {
		delete (*iter);
	}
	
	Paths.clear();
}

int Snapshot::parseHeader(string s)
{
	// Dedicated to parse <snapshot start=??? end=??? type=??? size=???> section, return [size]
	
	int p1 = s.find("start=", 0);
	if (p1 == string::npos) return -1;
	
	int p2 = s.find("end=", 0);
	if (p2 == string::npos) return -1;
	
	int p3 = s.find("type=", 0);
	if (p3 == string::npos) return -1;
	
	int p4 = s.find("size=", 0);
	if (p4 == string::npos) return -1;
	
	stringstream buf;
	string tmp;
	
	buf << s.substr(p1 + 6);
	buf >> TimeStart;
	
	buf.str("");
	buf << s.substr(p2 + 4);
	buf >> TimeEnd;
	
	int t;
	buf.str("");
	buf << s.substr(p3 + 5);
	buf >> t;

	switch(t) {
		case 1:	Type = LT_BEGIN; break;
		case 2: Type = LT_END; break;
		default: Type = LT_NORMAL; break;
	}
	
	int len;
	buf.str("");
	buf << s.substr(p4 + 5);
	buf >> len;
	
	return len;
}
