/*****************************************************************************
 *
 * SVGL - Sphinx Visualizer GL
 *
 * Bo Zhang, Engineering Dept, University of Waikato, New Zealand, 2007
 *
 ****************************************************************************/
 
#include <vector>
#include "Elements.hpp"

using namespace std;

#ifndef _SVGL_SNAPSHOT_
#define _SVGL_SNAPSHOT_

enum LAYER_TYPE {
	LT_NORMAL = 0x0,
	LT_BEGIN = 0x1,
	LT_END = 0x2
};

/* A snapshot contains multiple paths, the first one is the best */
class Snapshot {
	public: string toString();
	public: int parse(string &s);

	public: void setTimeStart(float t);
	public: void setTimeEnd(float t);
	public: float getTimeStart() const;
	public: float getTimeEnd() const;
	public: LAYER_TYPE getType() const;
	public: int size() const;
	public: const Path *getPath(int n);
	
	public: Snapshot();
	public: ~Snapshot();

	private: void clear();
	private: int parseHeader(string s);
	private: float	TimeStart;
	private: float	TimeEnd;
	private: LAYER_TYPE	Type;
	private: vector<Path *>	Paths;
};

#endif
