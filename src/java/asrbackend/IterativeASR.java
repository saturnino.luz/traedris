package asrbackend;

import edu.cmu.sphinx.frontend.util.Microphone;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;


/**
 * Iterative Recogniser to provide input to SVGL via SnapshotListener
 */
public class IterativeASR {

  /**
   * Main method for running the IterativeASR demo.
   */
  public static void main(String[] args) {
    try {
      URL url;
      if (args.length > 0) {
        url = new File(args[0]).toURI().toURL();
      } else {
        url = IterativeASR.class.getResource("iterativeasr.config.xml");
      }

      System.out.println("Loading...");

      ConfigurationManager cm = new ConfigurationManager(url);

      Recognizer recognizer = (Recognizer) cm.lookup("recognizer");
      Microphone microphone = (Microphone) cm.lookup("microphone");
      
      SnapshotListener sslistener = new SnapshotListener((new ServerSocket(4301)).accept());
      recognizer.addResultListener(sslistener);
      
      /* allocate the resource necessary for the recognizer */
      recognizer.allocate();
      printInstructions();
      
      /* the microphone will keep recording until the program exits */
      if (microphone.startRecording()) {
        
        while (true) {
          System.out.println
            ("Start speaking. Press Ctrl-C to quit.\n");
          
          /*
           * This method will return when the end of speech
           * is reached. Note that the endpointer will determine
           * the end of speech.
           */ 
          sslistener.startNewSentence();
          Result result = recognizer.recognize();
          
          if (result != null) {
            String resultText = result.getBestResultNoFiller();
            System.out.println("You said: " + resultText + "\n");
          } else {
            System.out.println("I can't hear what you said.\n");
          }
        }
      } else {
        System.out.println("Cannot start microphone.");
        recognizer.deallocate();
        System.exit(1);
      }
    } catch (IOException e) {
      System.err.println("Problem when loading IterativeASR: " + e);
      e.printStackTrace();
    } catch (PropertyException e) {
      System.err.println("Problem configuring IterativeASR: " + e);
      e.printStackTrace();
    } 
    //catch (InstantiationException e) {
    //  System.err.println("Problem creating IterativeASR: " + e);
    //  e.printStackTrace();
    // }
  }

  /**
   * Prints out what to say for this demo.
   */
  private static void printInstructions() {
    System.out.println
      ("Sample sentences:\n" +
       "\n" +
       "the green one right in the middle\n" +
       "the purple one on the lower right side\n" +
       "the closest purple one on the far left side\n" +
       "the only one left on the left\n" +
       "\n" +
       "Refer to the file iterativeasr.test for a complete list.\n");
  }
}
