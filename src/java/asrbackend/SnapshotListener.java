/**
 *  � 2007 S Luz <luzs@cs.tcd.ie>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/
package asrbackend;

import edu.cmu.sphinx.result.*;
import edu.cmu.sphinx.decoder.*;
import edu.cmu.sphinx.linguist.dictionary.Word;
import edu.cmu.sphinx.decoder.search.Token;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.PropertyException;
import java.lang.Math;
import java.io.*;
import java.net.*;
import java.util.*;


/**
 *  Get recognition result snapshots from sphinx-4 recognition backend
 *  and forward them to an IP socket, for reading and display by SVGL
 *  frontend
 *
 * @author  S Luz &#60;luzs@cs.tcd.ie&#62;
 * @version <font size=-1>$Id: $</font>
 * @see  
*/

public class SnapshotListener implements ResultListener {

  ServerSocket listener;
  Socket sock;
  PrintWriter writer;
  boolean listening = false;
  boolean isNewSentence = false;
  boolean dumpResults = false;
  boolean debug = false;
  int port = 4301;
  ArrayList paths = new ArrayList();
  int resultno = 0;

  public SnapshotListener(Socket s) throws IOException{
    sock = s;
    writer = new PrintWriter(sock.getOutputStream());
  }

  public SnapshotListener(OutputStream o) throws IOException{
    writer = new PrintWriter(o);
  }

  //////////// ResultListener interface ////////////
  public void newResult(Result result) {
    paths.clear();
    resultno++;
    if (result == null)
      return;
    if (debug)
      System.err.println("===SnapshotListener: RESULT==>"+result);
    Token tbest = result.getBestToken();
    paths.add(tokenToPath(tbest));
    List l = result.getResultTokens();
    for (Iterator i = l.iterator(); i.hasNext() ;) {
      Token t = (Token)i.next();
      Path p = tokenToPath(t);
      if (! pathExists(p))
        paths.add(p);
    }
    String s = snapshotToString();
    broadcast(s);
    if (dumpResults)
      System.out.println(s);
    if (debug)
      System.err.println("===SnapshotListener: SNAPSHOT==>"+s);
    //(new Lattice(result)).dumpAllPaths();
    //(new Lattice(result)).dump("/tmp/s"+resultno+".lat");
    //(new Lattice(result)).dumpDot("/tmp/s"+resultno+".dot", "Dot thing");
  }

  // This is now inherited from edu.cmu.sphinx.util.props.Configurable 
  public void newProperties(PropertySheet ps)
    throws PropertyException {

  }

  public void setDumpResults(boolean v){
    dumpResults = v;
  }
  public void setDebug(boolean v){
    debug = v;
  }

  public void startNewSentence(){
    isNewSentence = true;
  }

  public void broadcast(String msg){
    if (sock != null && writer != null) {
      if (sock.isConnected()) {
        writer.println(msg);
        writer.flush();
      }
      else {
        System.err.println("SnapshotListener: socket closed. Restart Listening...");
      }
    }
  }

  private Path tokenToPath (Token t){
    Path ret = new Path();
    while (t != null) {
      float sc = t.getScore();
      int time = t.getFrameNumber();
      if (t.getSearchState()!=null && t.isEmitting())
        time--;
      if (t.isWord()) {
        Word w = t.getWord();
        if (!w.isFiller()) {
          SVWord svw = new SVWord(time,w.getSpelling(),sc);
          ret.addWord(svw);
        }
      }
      t = t.getPredecessor();
    }
    return ret;
  }

  private String snapshotToString(){
    StringBuffer sb = new StringBuffer();
    int size = 0;
    for (Iterator i = paths.iterator(); i.hasNext() ;) {
      Path p = (Path)i.next();
      sb.append(p.toString());
      size++;
    }
    sb.append("</snapshot>");
    int type = isNewSentence ? 1 : 0;
    isNewSentence = false;
    sb.insert(0,"<snapshot start=0 end=0 type="+type+" size="+size+">");
    return sb.toString();
  }

  private boolean pathExists(Path pt){
    for (Iterator i = paths.iterator(); i.hasNext() ;) {
      Path p = (Path)i.next();
      if (pt.equals(p))
        return true;
    }
    return false;
  }


  ////////// INNER CLASSES /////////////////

  // private class ListenerThread implements Runnable {
  //   public void run(){
  //     try {
  //       listening = true;
  //       //sock = listener.accept();
  //       listener.close();
  //       writer = new PrintWriter(sock.getOutputStream());
  //       listening = false;
  //     } catch (Exception e) {
  //       e.printStackTrace();
  //     }
  //   }
  // }

  class SVWord {
    public int time;
    public String spell;
    public float score;
    
    public SVWord(int t, String sp, float sc){
      time = t;
      spell = sp;
      score = sc;
    }

    public boolean equals(SVWord s){
      if (Math.abs(time - s.time) > 20) 
        return false;
      if (!spell.equals(s.spell)) 
        return false;
      return true;
    }

    public String toString(){
      return "{"+time+","+spell+","+score+"}";
    }
  }

  class Path {
    ArrayList words = new ArrayList();
    
    public void addWord(SVWord s){
      words.add(s);
    }

    public void clear(){
      words.clear();
    }

    public SVWord get(int i){
      return (SVWord) words.get(i);
    }

    public int getSize(){
      return words.size();
    }

    public boolean equals(Path p){
      if (getSize() != p.getSize())
        return false;
      for (int i = 0; i < getSize(); i++) {
        if (!get(i).equals(p.get(i)))
          return false;
      }
      return true;
    }

    public String toString(){
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < getSize(); i++) {
        sb.insert(0,get(i).toString());
      }
      sb.append("</path>");
      sb.insert(0,"<path size="+getSize()+">");
      return sb.toString();
    }
  }
}
