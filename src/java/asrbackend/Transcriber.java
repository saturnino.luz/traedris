package asrbackend;

import edu.cmu.sphinx.frontend.util.StreamDataSource;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.frontend.*;
import edu.cmu.sphinx.frontend.endpoint.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;


import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;


/**
 * Transcribe file as requested by the dytread front-ent. 
 */
public class Transcriber extends Thread {
  public static final int PORT = 4301;

  Recognizer recognizer;
  FrontEnd frontend;
  SpeechClassifier speechClassifier;
	private final double sampleRate = 16000;
	private double speechStartTime = 0;
  SnapshotListener sslistener;
  //ServerSocket ssocket;
  boolean debug = true;
  

  public Transcriber (URL configURL, URL audioURL) 
    throws PropertyException, IOException, 
           InstantiationException, UnsupportedAudioFileException 
  {
    ConfigurationManager cm = new ConfigurationManager(configURL);
    recognizer = (Recognizer) cm.lookup("recognizer");

    //System.err.println("open socket");
    //ssocket = new ServerSocket(PORT);
    System.err.println("allocate");
    /* allocate the resource necessary for the recognizer */
    recognizer.allocate();
    System.err.println("recognizer allocated");
    AudioInputStream ais = AudioSystem.getAudioInputStream(audioURL);
    StreamDataSource reader = 
      (StreamDataSource) cm.lookup("streamDataSource");
    reader.setInputStream(ais, audioURL.getFile());
    System.err.println("audio input allocated: "+audioURL);

    frontend = (FrontEnd)cm.lookup("epFrontEnd");
    System.err.println("frontend set");
    speechClassifier = (SpeechClassifier)cm.lookup("speechClassifier");
    System.err.println("all resources allocated");

    frontend.addSignalListener(new SignalListener(){	 
        public void signalOccurred(Signal inSig){ 
          
          System.err.println("SS="+inSig);
          if(inSig instanceof SpeechStartSignal){            
            try{
              FloatData current = (FloatData)frontend.getData();					
              speechStartTime = current.getFirstSampleNumber();
							
              speechStartTime /= sampleRate;
              System.err.println("SPST="+speechStartTime);

            }catch(Exception e){
              e.printStackTrace();
            }
          }	
        }		
      });
  }

  public void run (){
    System.err.println("starting recogn");

    while (true){
      try {
        String ctl;
        //Socket sk = ssocket.accept();
        BufferedReader stdin =  new BufferedReader(new InputStreamReader(System.in));
        if (debug)
          System.err.println("connect");
        SnapshotListener sslistener = new SnapshotListener(System.out);
        recognizer.addResultListener(sslistener);
        boolean done = false;
        while (!done) {
          /*
           * This while loop will terminate after the last utterance
           * in the audio file has been decoded, in which case the
           * recognizer will return null.
           */ 
          sslistener.startNewSentence();
          Result result = recognizer.recognize();
          System.err.println("ST="+recognizer.getState());
          if (result != null) {
            String resultText = result.getBestResultNoFiller();
            if (debug)
              System.err.println("==RESULT====>"+resultText+"<===");
          } else {
            done = true;
          }
          if (debug)
            System.err.println("==Next???===");
          if (stdin.ready() && (ctl = stdin.readLine()) != null  ){
            if (debug)
              System.err.println("==COMMAND RECVD: "+ctl);
            if (ctl.equals("PAUSE")) {
              ctl = stdin.readLine();
              if (debug)
                System.err.println("==COMMAND RECVD: "+ctl);
            }
            if (ctl.equals("BYE"))
              done = true;
          }
          //          else{
          //  System.out.println("hit <ENTER>");
          //  stdin.readLine();
          //}
        }
      }
      catch(IOException e){
        System.err.println("Problem running recogniser " + e);
        e.printStackTrace();
      }
    }
  }

  /**
   * Main method for running the HelloDigits demo.
   */
  public static void main(String[] args) {
    try {
      URL audioURL;
      if (args.length > 0) {
        audioURL = new File(args[0]).toURI().toURL();
      } else {
        audioURL = 
          Transcriber.class.getResource("test1.wav");
      }
      
      URL configURL = Transcriber.class.getResource("transcriber.config.xml");
      
      Transcriber tcbr = new Transcriber(configURL, audioURL);
      tcbr.start();
    } catch (IOException e) {
      System.err.println("Problem when loading Transcriber: " + e);
      e.printStackTrace();
    } catch (PropertyException e) {
      System.err.println("Problem configuring Transcriber: " + e);
      e.printStackTrace();
    } catch (InstantiationException e) {
      System.err.println("Problem creating Transcriber: " + e);
      e.printStackTrace();
    } catch (UnsupportedAudioFileException e) {
	    System.err.println("Audio file format not supported.");
	    e.printStackTrace();
    } catch (Exception e) {
      System.err.println("Problem: " + e);
      e.printStackTrace();
    }
  }
}
